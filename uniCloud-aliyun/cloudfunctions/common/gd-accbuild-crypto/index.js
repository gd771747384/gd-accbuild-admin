const CryptoJS = require("crypto-js")
const defaultIv = CryptoJS.lib.WordArray.create(8);
const defaultKey = "xxxxxxxxx";
const genEncIv = () => {
	const iv = CryptoJS.lib.WordArray.random(16);
	return iv.toString(CryptoJS.enc.Base64)
}
const getIv = (ivGenStr) => {
	let iv = "";
	if (!ivGenStr) {
		iv = defaultIv;
	} else {
		iv = CryptoJS.enc.Base64.parse(ivGenStr)
	}
	return iv
};
const encrypt = (word, key = defaultKey, ivGenStr) => {
	return CryptoJS.DES.encrypt(word, key, {
		mode: CryptoJS.mode.CBC,
		iv: getIv(ivGenStr),
	}).toString();
};

const decrypt = (word, key = defaultKey, ivGenStr) => {
	return CryptoJS.DES.decrypt(word, key, {
		mode: CryptoJS.mode.CBC,
		iv: getIv(ivGenStr),
	}).toString(CryptoJS.enc.Utf8);
};
const getHashStr = (input, fixLen = 16) => {
	var hash = CryptoJS.SHA3(input, {
		outputLength: fixLen * 4
	});
	return hash.toString(CryptoJS.enc.Hex);
}
// //解密方法
// function decrypt(word) {
// 	let encryptedHexStr = CryptoJS.enc.Hex.parse(word);
// 	let srcs = CryptoJS.enc.Base64.stringify(encryptedHexStr);
// 	let decrypt = CryptoJS.AES.decrypt(srcs, key, {
// 		iv: iv,
// 		mode: CryptoJS.mode.CBC,
// 		padding: CryptoJS.pad.Pkcs7
// 	});
// 	let decryptedStr = decrypt.toString(CryptoJS.enc.Utf8);
// 	return decryptedStr.toString();
// }

// //加密方法
// function encrypt(word) {
// 	let srcs = CryptoJS.enc.Utf8.parse(word);
// 	let encrypted = CryptoJS.AES.encrypt(srcs, key, {
// 		iv: iv,
// 		mode: CryptoJS.mode.CBC,
// 		padding: CryptoJS.pad.Pkcs7
// 	});
// 	return encrypted.ciphertext.toString().toUpperCase();
// }
module.exports = {
	decrypt,
	encrypt,
	genEncIv,
	getIv,
	getHashStr
}