const nodemailer = require("nodemailer");
const generateId = require("generate-id")
async function main(verifycode, toEmail) {

	// 创建Nodemailer传输器 SMTP 或者 其他 运输机制
	let transporter = nodemailer.createTransport({
		service: 'qq', // 使用了内置传输发送邮件 查看支持列表：https://nodemailer.com/smtp/well-known/ 
		port: 465, // SMTP 端口 
		secureConnection: true, // 使用了 SSL
		auth: {
			user: "771747384@qq.com", // 发送方邮箱的账号
			pass: "*****", // 邮箱授权密码
		},
	});

	// 定义transport对象并发送邮件
	let info = await transporter.sendMail({
		from: '"优速搭(gd-AccBuild)" <771747384@qq.com>', // 发送方邮箱的账号
		to: toEmail, // 邮箱接受者的账号
		subject: "优速搭 账号注册", // Subject line
		text: "优速搭 账号注册", // 文本内容
		html: `<h1>欢迎注册 优速搭账号</h1>,<div>优速搭加速构建您的产品</div> <div>您的邮箱验证码是: <b>${verifycode}</b></div>`, // html 内容, 如果设置了html内容, 将忽略text内容
	});
	return Array.isArray(info.rejected) && info.rejected.length === 0
}
module.exports = {
	gdEmailSender: async function(toEmail) {
		const verifycode = await generateId()
		const isSuccess = await main(verifycode, toEmail);
		return {
			isSuccess,
			verifycode
		}
	},
	mailTool:null
}
