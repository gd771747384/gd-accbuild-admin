# gd-accbuild (优速搭)
## 开发环境注意事项
项目的编译运行要求                   nodejs >= 16.18.0
npx @dcloudio/uvm alpha             nodejs >= 16.18.0
需要提前安装 yarn 和 pnpm

## 安装依赖
点击 scripts/install.bat

## 初始化云空间、云函数、云数据库


## 点击HBuilderX 运行按钮--选择pc-admin

# 如何低代码混合开发
1. 前提安装HBuilderX的插件，[插件地址](https://ext.dcloud.net.cn/plugin?id=11368) <br/>
2. 将本项目导入到HBuilderX中，双击打开根目录下的 "优速搭设计器"文件
目前实现功能
- [X] 选择一个页面下载源码
- [X] 设计器中点击元数据字段，自动定位到导出的源码行
- [ ] 导出源码暂时预览使用尚未协同实时生效

# 如有问题可以查看文档或gitee上提问
[文档地址](http://gd771747384.gitee.io/gd-accbuild-doc/zh/guide/preface.html)

[设计器官网地址](http://designer.gd-accbuild.com)
