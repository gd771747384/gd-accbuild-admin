import { computed, reactive, watch, toRefs } from "vue"
//import { computed, watch, ref, reactive, inject } from "vue";
/**
 * 根据不同item容器:表单 / 表格,返回对应的绑定值
 */
export default (props,emits=()=>{}) => {
    //console.log('初始化绑定',props.metaItem.key,props.rowData,props)
    const containerType = computed(() =>
        props.tableBodyData === null && props.rowIdx === null && props.colIdx === null
            ? "Form"
            : "Table"
    );
    const curBindVal = computed({
        get: () => {
            if (containerType.value === "Form") {
                return props.formData[props.metaItem.key];
            } else {
                //return props.flattenTableBodyData[props.rowIdx][props.metaItem.key];
                return props.rowData[props.metaItem.key];
            }
        },
        set: (val) => {
            if (containerType.value === "Form") {
                props.formData[props.metaItem.key] = val;
            } else {
                //props.flattenTableBodyData[props.rowIdx][props.metaItem.key] = val;
                props.rowData[props.metaItem.key] = val;
            }
        },
    });
    /////设置默认值
    const curBindDefaultVal = computed(() => {
        return props.metaItem.default;
    });
    watch(
        () => curBindDefaultVal.value,
        (newVal, oldVal) => {
            if (newVal !== undefined && [undefined, null].includes(curBindVal.value)) {
                curBindVal.value = newVal;
            }
        },
        { immediate: true }
    );
    /////
    const obj = reactive({
        curBindVal,
        containerType,
    })
    return toRefs(obj)
}