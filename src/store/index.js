import { getCreateStore } from "@uni_modules/gd-accbuild-core/store/hackVueStore";

const modulesFiles = import.meta.glob("./modules/*.js", {
  eager: true,
  import: "default",
});
const pathReg = /^\.\/modules\/(.*)\.\w+$/;

const store = getCreateStore({
  modulesFiles,
  getModuleNameCallback: (modulePath) => modulePath.replace(pathReg, "$1"),
});
export default store;
