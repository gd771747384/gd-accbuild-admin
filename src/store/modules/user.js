import { login, wxLogin, logout } from '@/services/common'
export default {
	namespaced: true,
	state: {
		userInfo: uni.getStorageSync('userInfo') || {
			avatar:'https://sdn.geekzu.org/avatar/aff58a2634bd9cb24422b0b705fe678b?s=46&d=identicon',
			phone: '123456',
			name:'defaultName',
			nickName:'defaultNickName'
		},
		token: uni.getStorageSync('token') || '123'
	},
	mutations: {
		setUserInfo(state, userInfo) {
			state.userInfo = userInfo
			uni.setStorageSync('userInfo', userInfo)
		},
		setToken(state, token) {
			state.token = token
			uni.setStorageSync('token', token)
		}
	},
	actions: {
		login({
			commit
		}, data) {
			return login(data).then(res => {
				commit('setToken', res.token)
				commit('setUserInfo', res.userInfo)
			})
		},
		wxLogin({
			commit
		}, data) {
			return wxLogin(data).then(res => {
				commit('setToken', res.token)
				commit('setUserInfo', res.userInfo)
			})
		},
		logout({
			commit
		}) {
			// return http.post('auth/user/logout').then(res => {
			// 	commit('setToken', '')
			// 	commit('setUserInfo', {})
			// })
			return new Promise(resolve => {
				commit('setToken', '')
				commit('setUserInfo', {})
				resolve()
			})
		}
	}
}
