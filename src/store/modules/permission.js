import permissionStore from '@uni_modules/gd-accbuild-core/store/modules/permission.js'


const state = permissionStore.state

const getters = permissionStore.getters

const mutations = permissionStore.mutations

const actions = permissionStore.actions

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}
