import appStore from '@uni_modules/gd-accbuild-core/store/modules/pc-admin/app.js'

const state = appStore.state

const mutations = appStore.mutations

const actions = appStore.actions

export default {
    namespaced: true,
    state,
    mutations,
    actions
}
