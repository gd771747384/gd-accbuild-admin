import historyRouteStore from '@uni_modules/gd-accbuild-core/store/modules/historyRoute'


const state = historyRouteStore.state

const mutations = historyRouteStore.mutations

const actions = historyRouteStore.actions

export default {
    namespaced: true,
    state,
    mutations,
    actions
}
