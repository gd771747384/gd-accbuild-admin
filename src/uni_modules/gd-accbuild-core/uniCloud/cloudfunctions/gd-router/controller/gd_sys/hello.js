const { Controller } = require("uni-cloud-router");
module.exports = class HelloController extends Controller {
  async sayHello() {
    return await this.service.gd_sys.hello.sayHello();
  }
};