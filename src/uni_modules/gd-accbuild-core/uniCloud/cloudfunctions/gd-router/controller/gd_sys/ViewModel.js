const {
	Controller
} = require("uni-cloud-router");
module.exports = class ViewModelController extends Controller {
	getListByVmCode() {
		return this.service.gd_sys.ViewModel.getListByVmCode(this.ctx.event);
	}
};