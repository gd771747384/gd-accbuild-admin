const {
	Controller
} = require("uni-cloud-router");
module.exports = class EntityModelController extends Controller {
	async getListByVmCode() {
		return await this.service.gd_sys.EntityModel.getListByVmCode(this.ctx.event);
	}
	async initAutoIncreaseTableData() {
		return await this.service.gd_sys.EntityModel.initAutoIncreaseTableData(this.ctx.event);
	}
};