const {
	Controller
} = require("uni-cloud-router");
module.exports = class DictMgrController extends Controller {
	getListByVmCode() {
		return this.service.gd_sys.DictMgr.getListByVmCode(this.ctx.event);
	}
};
