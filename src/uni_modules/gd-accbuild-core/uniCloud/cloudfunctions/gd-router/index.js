// index.js (通常无需改动)做
const Router = require("uni-cloud-router").Router; // 引入 Router
const router = new Router(require("./config.js")); // 根据 config 初始化 Router
const jqlHandler = require('gd-accbuild-jql-handler')
exports.main = async (event, context) => {
	const dbJql = jqlHandler({
		event,
		context,
		env:"uniCloud"
	})
	event.dbJql = dbJql;//拦截参数并注入jql句柄
	console.log("gd-router参数拦截注入完成",event)
	return router.serve(event, context); // 由 Router 接管云函数
};
