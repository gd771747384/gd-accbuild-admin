const { Service } = require("uni-cloud-router");
const uniID = require('uni-id')
module.exports = class HelloService extends Service {
  async sayHello() {
	  console.log('进入hello service')
    return {
      data: "welcome to uni-cloud-router!",
    };
  }
};