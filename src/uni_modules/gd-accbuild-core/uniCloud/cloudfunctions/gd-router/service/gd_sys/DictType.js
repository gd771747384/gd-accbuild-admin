const {
	Service
} = require("uni-cloud-router");
const jqlHandler = require('gd-accbuild-jql-handler')
//const jqlPraser = require('gd-accbuild-jql-praser-from-sql')()
module.exports = class DictTypeService extends Service {
	async getListByVmCode({
		strategyName,
		params,
		data,
		vmCode: crudVmCode,
		dbJql
	}) {
		if (params.parentId === null) {
			const res = await dbJql.collection('gd_sys_own_module').get();
			return {
				errCode: res.code,
				errMsg: res.message,
				data: {
					data: {},
					items: res.data.map(el => ({
							"id": el.module_code,
							"parentId": null,
							"code": "",
							"name": el.display_name,
							"descrip": el.descrip,
							"ordinal": el.ordinal,
							"hasChildren":true
						}))
				}
			}
		} else {
			const res = await dbJql.collection('gd_sys_dict_type').where({
				"parent_id": params.parentId
			}).get();
			return {
				errCode: res.code,
				errMsg: res.message,
				data: {
					data: {},
					items: res.data.map(el => ({
							"id": el.id,
							"parentId": el.parent_id,
							"code": el.code,
							"name": el.name,
							"descrip": el.descrip,
							"ordinal": el.ordinal,
							"hasChildren":false
						}))
				}
			}
		}
	}
	async getByVmCode({
		strategyName,
		params,
		data,
		vmCode: crudVmCode,
		dbJql
	}) {
		return {
			errCode: 0,
			errMsg: "直传",
			isPassThrough:true,
			data: {
				data: {},
				items: []
			}
		}
	}
};
