const {
	Service
} = require("uni-cloud-router");
const jqlHandler = require('gd-accbuild-jql-handler')
//const jqlPraser = require('gd-accbuild-jql-praser-from-sql')()
module.exports = class ViewModelService extends Service {
	async getListByVmCode({
		strategyName,
		params,
		data,
		vmCode: crudVmCode,
		dbJql
	}) {
		if (params.parentId === null) {
			const res = await dbJql.collection('gd_sys_own_module').get();
			return {
				errCode: res.code,
				errMsg: res.message,
				data: {
					data: {},
					items: res.data.map(el => ({
						"id": el.module_code,
						"vmCode": "",
						"entityCode": "",
						"displayName": el.display_name,
						"ordinal": el.ordinal,
						"parentId": null,
						"hasChildren": true
					}))
				}
			}
		} else {
			const res = await dbJql.collection('gd_sys_view_model').where({
				"parent_id": params.parentId
			}).get();
			return {
				errCode: res.code,
				errMsg: res.message,
				data: {
					data: {},
					items: res.data.map(el => ({
						"id": el.id,
						"vmCode": el.vm_code,
						"entityCode": el.entity_code,
						"displayName": el.display_name,
						"ordinal": el.ordinal,
						"parentId": el.parent_id,
						"hasChildren": false
					}))
				}
			}
		}
	}
};
