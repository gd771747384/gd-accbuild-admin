const {
	Service
} = require("uni-cloud-router");
const jqlHandler = require('gd-accbuild-jql-handler')
//const jqlPraser = require('gd-accbuild-jql-praser-from-sql')()
module.exports = class MetaMgrService extends Service {
	async getListByVmCode({
		strategyName,
		params,
		data,
		vmCode: crudVmCode,
		dbJql
	}) {
		console.log(params, data, '查看数据======')
		if (params.isDistinct) {
			const dbRes = {message:"",data:[]}
			// const dbRes = await jqlPraser.transSqlToJqlPromise(
			// 	`SELECT DISTINCT vmCode FROM "gdAccBuild-Sys-MetaMgr";`, dbJql)
			return {
				errCode: dbRes.code,
				errMsg: dbRes.message,
				data: {
					items: dbRes.data
				}
			};
		} else {
			//直接查数据库
			return {
				errCode: 0,
				errMsg: "",
				data: {
					items: [],
				}
			};
		}
	}
};
