import { ref, computed } from "vue";
import { cloneDeep } from "@gd-accbuild-core/config/utils";
const containerProps = ref({
  isShowContainer: false,
  containerType: "Dialog",
  containerAttr: {},
  onDoOperation: ({ eventFrom, ...params }) => {},
});
export const maxMultiInstanceNum = ref(4);
export default (options) => {
  if (typeof options === "object" && Object.keys(options).length > 0) {
    /* Object.keys(options).forEach(key=>{
            containerProps.value[key] = options[key]
        }) */
    containerProps.value = options;
  }
  return containerProps;
};
export const multiInstanceContainerProps = ref({});
export const multiInstanceContainerKeys = ref([]);
export const setContainerProps = (options) => {
  if (typeof options === "object" && Object.keys(options).length > 0) {
    const isOverMaxNum =
      options.hasOwnProperty("key") && multiInstanceContainerKeys.length + 1 >= maxMultiInstanceNum.value;
    if (isOverMaxNum) {
      const removedKey = multiInstanceContainerKeys.value.shift();
      if (removedKey) {
        delete multiInstanceContainerProps.value[removedKey];
      }
    }
    if (options.hasOwnProperty("key")) {
      const isExistKey = multiInstanceContainerProps.value.hasOwnProperty(options.key);
      const containerDefaultProps = isExistKey
        ? multiInstanceContainerProps.value[options.key]
        : cloneDeep(containerProps.value);
      Object.keys(options).forEach((key) => {
        containerDefaultProps[key] = options[key];
      });
      multiInstanceContainerProps.value[options.key] = containerDefaultProps;
      !isExistKey && multiInstanceContainerKeys.value.push(options.key);
    } else {
      Object.keys(options).forEach((key) => {
        containerProps.value[key] = options[key];
      });
    }
  }
};

export const getContainerProps = (key) => {
  if (key) {
    return multiInstanceContainerProps.value[key];
  } else {
    return containerProps.value;
  }
};
export const toggleShowContainer = (key) => {
  /* console.log(
    multiInstanceContainerProps.value[key],
    "开始切换",
    multiInstanceContainerProps.value[key].isShowContainer
  ); */
  if (key) {
    multiInstanceContainerProps.value[key].isShowContainer = !multiInstanceContainerProps.value[key].isShowContainer;
  } else {
    containerProps.value.isShowContainer = !containerProps.value.isShowContainer;
  }
};
export const main = {
  getContainerProps,
  setContainerProps,
  toggleShowContainer,
};
