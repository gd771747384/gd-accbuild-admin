export default {
  mobile: {
    attrs: [
      {
        /**
         * 当pathInJson为undefined/"ComponentAttr"时,属性位于metaItem.componentAttr.里面
         * 当pathInJson为""时,属性位于metaItem.里面
         * 当pathInJson为"propOptions"时,属性位于propOptions  里面
         */
        pathInJson: "propOptions",
        uiConfigInPanel: {
          key: "isDefaultShow",
          label: "是否默认显示",
          type: "Switch",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "style",
          label: "容器style",
          type: "Input",
        },
      },
    ],
    rules: [],
    events: [
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "click",
          label: "click",
          type: "Adv-ButtonEmitCodeEditor",
        },
      },
    ],
  },
  pc: {
    attrs: [
      {
        /**
         * 当pathInJson为undefined/"ComponentAttr"时,属性位于metaItem.componentAttr.里面
         * 当pathInJson为""时,属性位于metaItem.里面
         * 当pathInJson为"propOptions"时,属性位于propOptions  里面
         */
        pathInJson: "propOptions",
        uiConfigInPanel: {
          key: "isDefaultShow",
          label: "是否默认显示",
          type: "Switch",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "style",
          label: "容器style",
          type: "Adv-ButtonEmitCodeEditor",
          default: "{}",
          componentAttr: {
            lang: "json",
          },
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "size",
          label: "尺寸",
          type: "Select",
          default: "default",
          options: [
            {
              label: "large",
              value: "large",
            },
            {
              label: "default",
              value: "default",
            },
            {
              label: "small",
              value: "small",
            },
          ],
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "disabled",
          label: "disabled",
          _isIndepenceInAddOrEdit: true, //默认新增编辑的表单相同;当此标记为true时,即新增编辑弹窗的表单不相同,保存后会自动替换为下面两行
          //labelCustomizeComp: markRaw(labelCustomizeComp),
          //itemContentCustomizeComp: markRaw(itemContentCustomizeComp),
          type: "Switch",
          default: false,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "text",
          label: "文字按钮",
          type: "Switch",
          default: false,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "icon",
          label: "图标",
          type: "Input",
          default: undefined,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "loading",
          label: "加载中",
          type: "Switch",
          default: undefined,
        },
      },
      //插槽配置;
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "defaultSlotConfig",
          label: "按钮内容",
          type: "Adv-StrinputWithJsondialog",
          componentAttr: {
            metaItemList: [],
          },
          //default: "按钮",//这个不要用用slotConfig。default代替;设计器配置的插槽默认值不推荐设置,如果存在默认值,会导致维度组件里面用vue-template 使用插槽会不生效
          //说明当前key在 维度组件/原子组件 内部是渲染为插槽的
          slotConfig: {
            slotName: "default",
          },
          //CustomizeComp (自定义组件统一放在static/components/customize/下面,后续在开发态自动会打包成本地组件)
          //contentType 一共三种类型：Text、IconPicker、AtomicComp、CustomizeComp;
          default: {
            contentType: "Text",
            value: "按钮",
          },
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "type",
          label: "type",
          type: "Select",
          default: undefined,
          options: [
            {
              label: "primary",
              value: "primary",
            },
            {
              label: "success",
              value: "success",
            },
            {
              label: "info",
              value: "info",
            },
            {
              label: "warning",
              value: "warning",
            },
            {
              label: "danger",
              value: "danger",
            },
            {
              label: "无",
              value: undefined,
            },
          ],
        },
      },
    ],
    rules: [],
    events: [
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "click",
          label: "click",
          type: "Adv-ButtonEmitCodeEditor",
          default: "",
          componentAttr: {
            textWrapperConfig: {
              prefixText: "function(configObj) {\n",
              suffixText: "}",
            },
          },
        },
      },
    ],
  },
};
