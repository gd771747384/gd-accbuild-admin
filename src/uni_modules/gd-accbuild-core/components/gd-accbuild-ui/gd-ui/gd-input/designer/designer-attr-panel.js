export default {
  mobile: {
    attrs: [
      {
        /**
         * 当pathInJson为undefined/"ComponentAttr"时,属性位于metaItem.componentAttr.里面
         * 当pathInJson为""时,属性位于metaItem.里面
         */
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "style",
          label: "容器style",
          type: "Input",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "type",
          label: "显示类型",
          type: "Select",
          default: "text",
          options: [
            {
              label: "text",
              value: "text",
            },
            {
              label: "textarea",
              value: "textarea",
            },
            {
              label: "password",
              value: "password",
            },
          ],
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "placeholder",
          label: "placeholder",
          type: "Input",
        },
      },
      {
        pathInJson: "",
        uiConfigInPanel: {
          key: "default",
          label: "默认值",
          type: "Input",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "size",
          label: "尺寸",
          type: "Select",
          default: "default",
          options: [
            {
              label: "large",
              value: "large",
            },
            {
              label: "default",
              value: "default",
            },
            {
              label: "small",
              value: "small",
            },
          ],
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "autosize",
          label: "autosize",
          type: "Switch",
          default: false,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "maxlength",
          label: "maxlength",
          type: "Input",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "readonly",
          label: "readonly",
          type: "Switch",
          default: false,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "disabled",
          label: "disabled",
          type: "Switch",
          default: false,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "showWordLimit",
          label: "统计字数提示",
          type: "Switch",
          default: false,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "clearable",
          label: "清除按钮",
          type: "Switch",
          default: false,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "showPassword",
          label: "密码显隐按钮",
          type: "Switch",
          default: false,
        },
      },
    ],
    rules: [],
    events: [
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "blur",
          label: "blur",
          type: "Input",
        },
      },
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "focus",
          label: "focus",
          type: "Input",
        },
      },
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "change",
          label: "change",
          type: "Input",
        },
      },
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "input",
          label: "input",
          type: "Input",
        },
      },
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "clear",
          label: "clear",
          type: "Input",
        },
      },
    ],
  },
  pc: {
    attrs: [
      {
        /**
         * 当pathInJson为undefined/"ComponentAttr"时,属性位于metaItem.componentAttr.里面
         * 当pathInJson为""时,属性位于metaItem.里面
         * 当pathInJson为"propOptions"时,属性位于propOptions  里面
         */
        pathInJson: "propOptions",
        uiConfigInPanel: {
          key: "isDefaultShow",
          label: "是否默认显示",
          type: "Switch",
          default: true,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "style",
          label: "容器style",
          type: "Adv-ButtonEmitCodeEditor",
          default: "{}",
          componentAttr: {
            lang: "json",
          },
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "type",
          label: "显示类型",
          type: "Select",
          default: "text",
          options: [
            {
              label: "text",
              value: "text",
            },
            {
              label: "number",
              value: "number",
            },
            {
              label: "textarea",
              value: "textarea",
            },
            {
              label: "password",
              value: "password",
            },
          ],
          events: (configObj) => {
            return {
              change: (val) => {
                const setMetaItemListShowStatus = (curVal) => {
                  configObj.metaItemList.forEach((el) => {
                    if (el.key === "showPassword") {
                      if ("password" === curVal) {
                        el.isShow = true;
                      } else {
                        el.isShow = false;
                      }
                    } else if (el.key === "size") {
                      if ("textarea" !== curVal) {
                        el.isShow = true;
                      } else {
                        el.isShow = false;
                      }
                    } else if (["rows", "autosize", "showWordLimit"].includes(el.key)) {
                      if ("textarea" === curVal) {
                        el.isShow = true;
                      } else {
                        el.isShow = false;
                      }
                    }
                  });
                };
                setMetaItemListShowStatus(val);

                //联动设置级联组件
                const uiOptionsStaticConfig = configObj["formData"]["__extra"]["rowData"]["uiOptionsStaticConfig"];
                if (!uiOptionsStaticConfig.hasOwnProperty("componentAttr")) {
                  uiOptionsStaticConfig.componentAttr = {};
                }
                uiOptionsStaticConfig.componentAttr["type"] = val;
              },
            };
          },
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "placeholder",
          label: "placeholder",
          type: "Input",
          default: "",
        },
      },
      {
        pathInJson: "",
        uiConfigInPanel: {
          key: "default",
          label: "默认值",
          type: "Input",
          default: "",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "size",
          label: "尺寸",
          type: "Select",
          default: "default",
          options: [
            {
              label: "large",
              value: "large",
            },
            {
              label: "default",
              value: "default",
            },
            {
              label: "small",
              value: "small",
            },
          ],
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "autosize",
          label: "autosize",
          type: "Switch",
          default: false,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "maxlength",
          label: "maxlength",
          type: "Input",
          default: undefined,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "rows",
          label: "rows",
          type: "Input",
          componentAttr: { type: "number" },
          default: undefined,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "readonly",
          label: "readonly",
          _isIndepenceInAddOrEdit: true, //默认新增编辑的表单相同;当此标记为true时,即新增编辑弹窗的表单不相同,保存后会自动替换为下面两行
          //labelCustomizeComp: markRaw(labelCustomizeComp),
          //itemContentCustomizeComp: markRaw(itemContentCustomizeComp),
          type: "Switch",
          default: false,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "disabled",
          label: "disabled",
          _isIndepenceInAddOrEdit: true, //默认新增编辑的表单相同;当此标记为true时,即新增编辑弹窗的表单不相同,保存后会自动替换为下面两行
          //labelCustomizeComp: markRaw(labelCustomizeComp),
          //itemContentCustomizeComp: markRaw(itemContentCustomizeComp),
          type: "Switch",
          default: false,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "showWordLimit",
          label: "统计字数提示",
          type: "Switch",
          default: false,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "clearable",
          label: "清除按钮",
          type: "Switch",
          default: false,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "showPassword",
          label: "密码显隐按钮",
          type: "Switch",
          default: false,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "prefixSlotConfig",
          label: "前缀插槽",
          type: "Adv-StrinputWithJsondialog",
          componentAttr: {
            metaItemList: [],
          },
          //说明当前key在 维度组件/原子组件 内部是渲染为插槽的
          slotConfig: {
            slotName: "prefix",
          },
          //CustomizeComp (自定义组件统一放在static/components/customize/下面,后续在开发态自动会打包成本地组件)
          //contentType 一共三种类型：Text、IconPicker、AtomicComp、CustomizeComp;
          default: {},
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "appendSlotConfig",
          label: "后缀插槽",
          type: "Adv-StrinputWithJsondialog",
          componentAttr: {
            metaItemList: [],
          },
          //说明当前key在 维度组件/原子组件 内部是渲染为插槽的
          slotConfig: {
            slotName: "append",
          },
          //CustomizeComp (自定义组件统一放在static/components/customize/下面,后续在开发态自动会打包成本地组件)
          //contentType 一共三种类型：Text、IconPicker、AtomicComp、CustomizeComp;
          default: {},
        },
      },
    ],
    rules: [],
    events: [
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "blur",
          label: "blur",
          type: "Adv-ButtonEmitCodeEditor",
          default: "",
          componentAttr: {
            textWrapperConfig: {
              prefixText: "function(configObj) {\n",
              suffixText: "}",
            },
          },
        },
      },
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "focus",
          label: "focus",
          type: "Adv-ButtonEmitCodeEditor",
          default: "",
          componentAttr: {
            textWrapperConfig: {
              prefixText: "function(configObj) {\n",
              suffixText: "}",
            },
          },
        },
      },
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "change",
          label: "change",
          type: "Adv-ButtonEmitCodeEditor",
          default: "",
          componentAttr: {
            textWrapperConfig: {
              prefixText: "function(configObj) {\n",
              suffixText: "}",
            },
          },
        },
      },
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "input",
          label: "input",
          type: "Adv-ButtonEmitCodeEditor",
          default: "",
          componentAttr: {
            textWrapperConfig: {
              prefixText: "function(configObj) {\n",
              suffixText: "}",
            },
          },
        },
      },
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "clear",
          label: "clear",
          type: "Adv-ButtonEmitCodeEditor",
          default: "",
          componentAttr: {
            textWrapperConfig: {
              prefixText: "function(configObj) {\n",
              suffixText: "}",
            },
          },
        },
      },
    ],
  },
};
