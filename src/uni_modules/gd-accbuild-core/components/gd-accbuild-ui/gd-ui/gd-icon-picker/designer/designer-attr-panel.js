export default {
  mobile: {
    attrs: [
      {
        /**
         * 当pathInJson为undefined/"ComponentAttr"时,属性位于metaItem.componentAttr.里面
         * 当pathInJson为""时,属性位于metaItem.里面
         * 当pathInJson为"propOptions"时,属性位于propOptions  里面
         */
        pathInJson: "propOptions",
        uiConfigInPanel: {
          key: "isDefaultShow",
          label: "是否默认显示",
          type: "Switch",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "style",
          label: "容器style",
          type: "Input",
        },
      },
    ],
    rules: [],
    events: [
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "click",
          label: "click",
          type: "Adv-ButtonEmitCodeEditor",
        },
      },
    ],
  },
  pc: {
    attrs: [
      {
        /**
         * 当pathInJson为undefined/"ComponentAttr"时,属性位于metaItem.componentAttr.里面
         * 当pathInJson为""时,属性位于metaItem.里面
         * 当pathInJson为"propOptions"时,属性位于propOptions  里面
         */
        pathInJson: "propOptions",
        uiConfigInPanel: {
          key: "isDefaultShow",
          label: "是否默认显示",
          type: "Switch",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "style",
          label: "容器style",
          type: "Adv-ButtonEmitCodeEditor",
          default: "{}",
          componentAttr: {
            lang: "json",
          },
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "size",
          label: "尺寸",
          type: "Select",
          default: "default",
          options: [
            {
              label: "large",
              value: "large",
            },
            {
              label: "default",
              value: "default",
            },
            {
              label: "small",
              value: "small",
            },
          ],
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "width",
          label: "width",
          type: "Input",
          default: "300px",
          componentAttr: {
            type: "text",
          },
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "isShowContainer",
          label: "isShowContainer",
          type: "Switch",
          default: false,
          vModelFlag: true, // 除默认modelValue外的双向绑定的标记;
          isShow: false,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "layoutConfig",
          label: "高级配置",
          type: "Adv-ButtonEmitCodeEditor",
          default: {
            /**
             * 布局类型
             * "Simple" 简化版没有左树、"Advance" 高级版有左树
             */
            type: "Advance",
            /**
             * 收藏夹分类
             * "build-in"是内置icon: pc的el-icon
             */
            favClassify: ["build-in", "material-symbols"],
            maxFavCacheNum: 5,
          },
          vModelFlag: true, // 除默认modelValue外的双向绑定的标记;
          componentAttr: {
            lang: "json",
          },
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "virtualTriggerShow",
          label: "是否虚拟触发",
          type: "Switch",
          default: null,
          vModelFlag: true, // 除默认modelValue外的双向绑定的标记;
          isShow: false,
        },
      },
    ],
    rules: [],
    events: [
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "change",
          label: "change",
          type: "Adv-ButtonEmitCodeEditor",
          default: "",
          componentAttr: {
            textWrapperConfig: {
              prefixText: "function(configObj) {\n",
              suffixText: "}",
            },
          },
        },
      },
    ],
  },
};
