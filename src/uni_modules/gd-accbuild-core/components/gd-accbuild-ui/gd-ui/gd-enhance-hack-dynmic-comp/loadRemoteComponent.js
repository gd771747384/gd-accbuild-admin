import * as Vue from "vue";
import genFromCode from "./preprocess/utils";
import {
  loadModule,
  //vueVersion
} from "vue3-sfc-loader"; // /dist/vue3-sfc-loader.esm.js";
export default async (url) => {
  console.log('执行远程组件')
  const res = await fetch(
    `${url}?random=${Math.floor(Math.random() * (1 - 1000)) + 1000}`
  );
  const code = await res.text();
  const { source, injectComp } = await genFromCode(code);
  //console.log(source,injectComp,'解析结果=====')
  return loadModule(url, {
    moduleCache: {
      vue: Vue,
      injectComp,
    },
    /* customBlockHandler(block, filename, options) {
      if (!block.type.startsWith("el-")) return;
      console.log("element-ui", block, filename, options);
    }, */
    getFile: async (url) => {
      return Promise.resolve(source);
    },
    addStyle() {
      /* unused here */
    },
  });
  //return remoteComp;
};
