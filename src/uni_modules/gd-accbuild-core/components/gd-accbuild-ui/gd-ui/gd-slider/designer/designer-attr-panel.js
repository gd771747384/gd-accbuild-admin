export default {
  mobile: {
    attrs: [
      {
        /**
         * 当pathInJson为undefined/"ComponentAttr"时,属性位于metaItem.componentAttr.里面
         * 当pathInJson为""时,属性位于metaItem.里面
         * 当pathInJson为"propOptions"时,属性位于propOptions  里面
         */
        pathInJson: "propOptions",
        uiConfigInPanel: {
          key: "isDefaultShow",
          label: "是否默认显示",
          type: "Switch",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "style",
          label: "容器style",
          type: "Input",
        },
      },
    ],
    rules: [],
    events: [
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "click",
          label: "click",
          type: "Adv-ButtonEmitCodeEditor",
        },
      },
    ],
  },
  pc: {
    attrs: [
      {
        /**
         * 当pathInJson为undefined/"ComponentAttr"时,属性位于metaItem.componentAttr.里面
         * 当pathInJson为""时,属性位于metaItem.里面
         * 当pathInJson为"propOptions"时,属性位于propOptions  里面
         */
        pathInJson: "propOptions",
        uiConfigInPanel: {
          key: "isDefaultShow",
          label: "是否默认显示",
          type: "Switch",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "style",
          label: "容器style",
          type: "Adv-ButtonEmitCodeEditor",
          default: "{}",
          componentAttr: {
            lang: "json",
          },
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "size",
          label: "尺寸",
          type: "Select",
          default: "default",
          options: [
            {
              label: "large",
              value: "large",
            },
            {
              label: "default",
              value: "default",
            },
            {
              label: "small",
              value: "small",
            },
          ],
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "disabled",
          label: "disabled",
          type: "Switch",
          default: false,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "min",
          label: "min",
          type: "Input",
          componentAttr: {
            type: "number",
          },
          default: 0,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "max",
          label: "max",
          type: "Input",
          componentAttr: {
            type: "number",
          },
          default: 100,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "step",
          label: "step",
          type: "Input",
          componentAttr: {
            type: "number",
          },
          default: 1,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "show-input",
          label: "show-input",
          type: "Switch",
          default: false,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "show-input-controls",
          label: "show-input-controls",
          type: "Switch",
          default: true,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "show-stops",
          label: "show-stops",
          type: "Switch",
          default: false,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "show-tooltip",
          label: "show-tooltip",
          type: "Switch",
          default: true,
        },
      } /* {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "format-tooltip",
                label: "format-tooltip",
                type: "Adv-ButtonEmitCodeEditor",
                default: false
            }
        }, */,
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "range",
          label: "range",
          type: "Switch",
          default: false,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "vertical",
          label: "vertical",
          type: "Switch",
          default: false,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "height",
          label: "height",
          type: "Input",
          default: undefined,
        },
      },
    ],
    rules: [],
    events: [
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "change",
          label: "change",
          type: "Adv-ButtonEmitCodeEditor",
          default: "",
          componentAttr: {
            textWrapperConfig: {
              prefixText: "function(configObj) {\n",
              suffixText: "}",
            },
          },
        },
      },
    ],
  },
};
