
export default {
    "mobile": {
        attrs: [{
            /**
             * 当pathInJson为undefined/"ComponentAttr"时,属性位于metaItem.componentAttr.里面
             * 当pathInJson为""时,属性位于metaItem.里面
             */
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "style",
                label: "容器style",
                type: "Input",
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "type",
                label: "显示类型",
                type: "Select",
                default: "datetime",
                options: [{
                    label: "datetime",
                    value: "datetime"
                }, {
                    label: "datetimerange",
                    value: "datetimerange"
                }, {
                    label: "daterange",
                    value: "daterange"
                }, {
                    label: "year",
                    value: "year"
                }, {
                    label: "month",
                    value: "month"
                }, {
                    label: "date",
                    value: "date"
                }, {
                    label: "week",
                    value: "week"
                }],
                events: (configObj) => {
                    return {
                        change: (val) => {
                            const setMetaItemListShowStatus = (typeVal) => {
                                const matchedKeys = ["placeholder", "startPlaceholder", "endPlaceholder", "rangeSeparator"]
                                configObj.metaItemList.forEach(el => {
                                    if (matchedKeys.includes(el.key)) {
                                        if (typeVal.endsWith("range")) {
                                            el.isShow = ["startPlaceholder", "endPlaceholder", "range-separator"].includes(el.key)
                                        } else {
                                            el.isShow = ["placeholder"].includes(el.key)
                                        }

                                    }
                                })
                            }
                            setMetaItemListShowStatus(val)

                            //联动设置级联组件
                            const uiOptionsStaticConfig = configObj["formData"]["__extra"]["rowData"]["uiOptionsStaticConfig"];
                            if (!uiOptionsStaticConfig.hasOwnProperty("componentAttr")) {
                                uiOptionsStaticConfig.componentAttr = {}
                            }
                            uiOptionsStaticConfig.componentAttr["type"] = val
                        }
                    }
                }
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "placeholder",
                label: "placeholder",
                type: "Input",
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "startPlaceholder",
                label: "startPlaceholder",
                type: "Input",
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "endPlaceholder",
                label: "endPlaceholder",
                type: "Input",
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "rangeSeparator",
                label: "rangeSeparator",
                type: "Input",
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "format",
                label: "format",
                type: "Input",
                default: "YYYY-MM-DD HH:mm:ss"
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "valueFormat",
                label: "valueFormat",
                type: "Input",
                default: "YYYY-MM-DD HH:mm:ss"
            }
        }, {
            pathInJson: "",
            uiConfigInPanel: {
                key: "default",
                label: "默认值",
                type: "Input",
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "size",
                label: "尺寸",
                type: "Select",
                default: "default",
                options: [{
                    label: "large",
                    value: "large"
                }, {
                    label: "default",
                    value: "default"
                }, {
                    label: "small",
                    value: "small"
                }]
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "readonly",
                label: "readonly",
                type: "Switch",
                default: false
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "disabled",
                label: "disabled",
                type: "Switch",
                default: false
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "clearable",
                label: "清除按钮",
                type: "Switch",
                default: false
            }
        }],
        rules: [],
        events: [{
            pathInJson: "events",
            uiConfigInPanel: {
                key: "blur",
                label: "blur",
                type: "Input",
            }
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "focus",
                label: "focus",
                type: "Input",
            }
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "change",
                label: "change",
                type: "Input",
            }
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "input",
                label: "input",
                type: "Input",
            }
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "clear",
                label: "clear",
                type: "Input",
            }
        }]
    },
    "pc": {
        /* type: "datetime",
  placeholder: "",
  startPlaceholder: "开始",
  endPlaceholder: "结束",
  format: "YYYY-MM-DD HH:mm:ss",
  //defaultValue
  rangeSeparator: "-",
  defaultTime: undefined,
  valueFormat: undefined,
  clearable: false,
  teleported: false,
  //继承属性
  size: "default",
  readonly: false,
  disabled: false, */
        attrs: [{
            /**
             * 当pathInJson为undefined/"ComponentAttr"时,属性位于metaItem.componentAttr.里面
             * 当pathInJson为""时,属性位于metaItem.里面
             * 当pathInJson为"propOptions"时,属性位于propOptions  里面
             */
            pathInJson: "propOptions",
            uiConfigInPanel: {
                key: "isDefaultShow",
                label: "是否默认显示",
                type: "Switch",
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "style",
                label: "容器style",
                type: "Adv-ButtonEmitCodeEditor",
                default: "{}",
                componentAttr: {
                    lang: "json"
                }
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "type",
                label: "日期时间类型",
                type: "Select",
                default: "datetime",
                options: [{
                    label: "datetime",
                    value: "datetime"
                }, {
                    label: "year",
                    value: "year"
                }, {
                    label: "month",
                    value: "month"
                }, {
                    label: "date",
                    value: "date"
                }, {
                    label: "week",
                    value: "week"
                }, {
                    label: "datetimerange",
                    value: "datetimerange"
                }, {
                    label: "daterange",
                    value: "daterange"
                }],
                events: (configObj) => {
                    return {
                        change: (val) => {
                            const setMetaItemListShowStatus = (curVal) => {
                                configObj.metaItemList.forEach(el => {
                                    if (["startPlaceholder", "endPlaceholder", "rangeSeparator"].includes(el.key)) {
                                        if (curVal.endsWith("range")) {
                                            el.isShow = true
                                        } else {
                                            el.isShow = false
                                        }
                                    } else if (el.key === "placeholder") {
                                        if (curVal.endsWith("range")) {
                                            el.isShow = false
                                        } else {
                                            el.isShow = true
                                        }
                                    } else if (el.key === "defaultTime") {
                                        if (curVal.indexOf("time") !== -1) {
                                            el.isShow = true
                                        } else {
                                            el.isShow = false
                                        }
                                    }
                                })
                            }
                            setMetaItemListShowStatus(val)

                            //联动设置级联组件
                            const uiOptionsStaticConfig = configObj["formData"]["__extra"]["rowData"]["uiOptionsStaticConfig"];
                            if (!uiOptionsStaticConfig.hasOwnProperty("componentAttr")) {
                                uiOptionsStaticConfig.componentAttr = {}
                            }
                            uiOptionsStaticConfig.componentAttr["type"] = val
                        }
                    }
                }
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "format",
                label: "format",
                type: "Input",
                default: "YYYY-MM-DD HH:mm:ss"
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "valueFormat",
                label: "valueFormat",
                type: "Input",
                default: "YYYY-MM-DD HH:mm:ss"
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "placeholder",
                label: "placeholder",
                type: "Input"
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "startPlaceholder",
                label: "startPlaceholder",
                type: "Input",
                default: "开始"
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "endPlaceholder",
                label: "endPlaceholder",
                type: "Input",
                default: "结束"
            }
        }, {
            pathInJson: "",
            uiConfigInPanel: {
                key: "default",
                label: "默认值",
                type: "Input",
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "rangeSeparator",
                label: "rangeSeparator",
                type: "Input",
                default: "-"
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "defaultTime",
                label: "defaultTime",
                type: "Input",
                default: undefined
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "teleported",
                label: "teleported",
                type: "Switch",
                default: false
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "size",
                label: "尺寸",
                type: "Select",
                default: "default",
                options: [{
                    label: "large",
                    value: "large"
                }, {
                    label: "default",
                    value: "default"
                }, {
                    label: "small",
                    value: "small"
                }]
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "readonly",
                label: "readonly",
                _isIndepenceInAddOrEdit: true,//默认新增编辑的表单相同;当此标记为true时,即新增编辑弹窗的表单不相同,保存后会自动替换为下面两行
                //labelCustomizeComp: markRaw(labelCustomizeComp),
                //itemContentCustomizeComp: markRaw(itemContentCustomizeComp),
                type: "Switch",
                default: false
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "disabled",
                label: "disabled",
                _isIndepenceInAddOrEdit: true,//默认新增编辑的表单相同;当此标记为true时,即新增编辑弹窗的表单不相同,保存后会自动替换为下面两行
                //labelCustomizeComp: markRaw(labelCustomizeComp),
                //itemContentCustomizeComp: markRaw(itemContentCustomizeComp),
                type: "Switch",
                default: false
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "clearable",
                label: "清除按钮",
                type: "Switch",
                default: false
            }
        }],
        rules: [],
        events: [{
            pathInJson: "events",
            uiConfigInPanel: {
                key: "blur",
                label: "blur",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "focus",
                label: "focus",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "change",
                label: "change",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "input",
                label: "input",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "clear",
                label: "clear",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }]
    }
}