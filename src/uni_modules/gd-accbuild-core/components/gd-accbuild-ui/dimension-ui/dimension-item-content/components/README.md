# 文件夹介绍
此文件夹列出的内嵌原子组件 是 用于 CRUD容器内部渲染的。
此文件夹不用手动编辑。只需要在原子组件提交,且配置为是否能被CRUD组件内嵌,则这边自动生成

xxx-components文件夹为用户的自定义组件,xxx为命名空间

其它文件夹是优速搭官方提供的组件

## 文件夹内部文件组成
如 
input
|
|-------input.vue  具体原子组件逻辑
|-------designer-attr-panel.js 设计器的属性面板 当选择该原子组件时的可视化配置项
|-------designer-comp-types.json 设计器的左侧组件列表中显示的图标、名称等,以及在

[读取url文件的内容](https://blog.csdn.net/qq_33878858/article/details/107317494)
IndexedDB 存储
