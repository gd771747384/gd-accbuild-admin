export default {
  mobile: {
    attrs: [
      {
        /**
         * 当pathInJson为undefined/"ComponentAttr"时,属性位于metaItem.componentAttr.里面
         * 当pathInJson为""时,属性位于metaItem.里面
         * 当pathInJson为"propOptions"时,属性位于propOptions  里面
         */
        pathInJson: "propOptions",
        uiConfigInPanel: {
          key: "isDefaultShow",
          label: "是否默认显示",
          type: "Switch",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "style",
          label: "容器style",
          type: "Input",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "type",
          label: "显示类型",
          type: "Select",
          default: "text",
          options: [
            {
              label: "text",
              value: "text",
            },
            {
              label: "textarea",
              value: "textarea",
            },
            {
              label: "password",
              value: "password",
            },
          ],
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "placeholder",
          label: "placeholder",
          type: "Input",
        },
      },
      {
        pathInJson: "",
        uiConfigInPanel: {
          key: "default",
          label: "默认值",
          type: "Input",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "size",
          label: "尺寸",
          type: "Select",
          default: "default",
          options: [
            {
              label: "large",
              value: "large",
            },
            {
              label: "default",
              value: "default",
            },
            {
              label: "small",
              value: "small",
            },
          ],
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "autosize",
          label: "autosize",
          type: "Switch",
          default: false,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "maxlength",
          label: "maxlength",
          type: "Input",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "readonly",
          label: "readonly",
          type: "Switch",
          default: false,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "disabled",
          label: "disabled",
          type: "Switch",
          default: false,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "showWordLimit",
          label: "统计字数提示",
          type: "Switch",
          default: false,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "clearable",
          label: "清除按钮",
          type: "Switch",
          default: false,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "showPassword",
          label: "密码显隐按钮",
          type: "Switch",
          default: false,
        },
      },
    ],
    rules: [],
    events: [
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "blur",
          label: "blur",
          type: "Adv-ButtonEmitCodeEditor",
        },
      },
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "focus",
          label: "focus",
          type: "Adv-ButtonEmitCodeEditor",
        },
      },
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "change",
          label: "change",
          type: "Adv-ButtonEmitCodeEditor",
        },
      },
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "input",
          label: "input",
          type: "Adv-ButtonEmitCodeEditor",
        },
      },
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "clear",
          label: "clear",
          type: "Adv-ButtonEmitCodeEditor",
        },
      },
    ],
  },
  pc: {
    attrs: [
      {
        /**
         * 当pathInJson为undefined/"ComponentAttr"时,属性位于metaItem.componentAttr.里面
         * 当pathInJson为""时,属性位于metaItem.里面
         * 当pathInJson为"propOptions"时,属性位于propOptions  里面
         */
        pathInJson: "propOptions",
        uiConfigInPanel: {
          key: "isDefaultShow",
          label: "是否默认显示",
          type: "Switch",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "style",
          label: "容器style",
          type: "Input",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "multiple",
          label: "是否多选",
          type: "Switch",
          default: "false",
          events: (configObj) => {
            return {
              change: (val) => {
                const setMetaItemListShowStatus = (multipleVal) => {
                  const matchedKeys = ["collapseTags", "collapseTagsTooltip", "multipleLimit"];
                  configObj.metaItemList.forEach((el) => {
                    if (matchedKeys.includes(el.key)) {
                      if (multipleVal) {
                        el.isShow = true;
                      } else {
                        el.isShow = false;
                      }
                    }
                  });
                };
                setMetaItemListShowStatus(val);

                //联动设置级联组件
                const uiOptionsStaticConfig = configObj["formData"]["__extra"]["rowData"]["uiOptionsStaticConfig"];
                if (!uiOptionsStaticConfig.hasOwnProperty("componentAttr")) {
                  uiOptionsStaticConfig.componentAttr = {};
                }
                uiOptionsStaticConfig.componentAttr["multiple"] = val;
              },
            };
          },
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "collapseTags",
          label: "多选时是否堆叠",
          type: "Switch",
          default: "true",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "collapseTagsTooltip",
          label: "多选时是否堆叠提示",
          type: "Switch",
          default: "true",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "multipleLimit",
          label: "多选限制数",
          type: "Input",
          default: 0,
        },
      },
      {
        pathInJson: "",
        uiConfigInPanel: {
          key: "placeholder",
          label: "placeholder",
          type: "Input",
        },
      },
      {
        pathInJson: "",
        uiConfigInPanel: {
          key: "default",
          label: "默认值",
          type: "Input",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "size",
          label: "尺寸",
          type: "Select",
          default: "default",
          options: [
            {
              label: "large",
              value: "large",
            },
            {
              label: "default",
              value: "default",
            },
            {
              label: "small",
              value: "small",
            },
          ],
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "disabled",
          label: "disabled",
          _isIndepenceInAddOrEdit: true,
          type: "Switch",
          default: false,
        },
      },
      {
        pathInJson: "dynmicConfig",
        uiConfigInPanel: {
          key: "optionsConfig",
          label: "配置选项",
          type: "Adv-ButtonEmitCodeEditor",
          componentAttr: {
            lang: "json",
            pickedDefaultCodeAdviceVal: "DictData",
            codeAdviceOptions: [
              {
                label: "来自数据库字典",
                value: "DictData",
                data: {
                  dataFromType: "DictData",
                  params: {
                    dictTypeCode: "resource_type",
                  },
                  labelKeyAlias: "name",
                  valueKeyAlias: "value",
                },
              },
              {
                label: "来自静态文本",
                value: "Static",
                data: {
                  dataFromType: "Static",
                  options: [
                    {
                      label: "选项a",
                      value: "a",
                    },
                    {
                      label: "选项b",
                      value: "b",
                    },
                  ],
                },
              },
              {
                label: "来自getList接口",
                value: "VmCode",
                data: {
                  dataFromType: "VmCode",
                  vmCode: "Resource",
                  params: {},
                  labelKeyAlias: "name",
                  valueKeyAlias: "id",
                },
              },
              {
                label: "来自Api接口",
                value: "Api",
                data: {
                  dataFromType: "Api",
                },
              },
            ],
          },
        },
      },
      /* {
            pathInJson: "dynmicConfig.optionsConfig.dataFromType",
            uiConfigInPanel: {
                key: "options",
                label: "选项数据源",
                type: "Select",
                default: [],
                options: [{
                    label: "数据库字典",
                    value: "DictData"
                }, {
                    label: "静态文本",
                    value: "Static"
                }, {
                    label: "getList接口",
                    value: "VmCode"
                }, {
                    label: "Api接口",
                    value: "Api"
                }]
            }
        }, {
            pathInJson: "",
            uiConfigInPanel: {
                key: "options",
                label: "选项",
                type: "Select",
                default: []
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "customizeOptionStyle",
                label: "下拉选项风格",
                type: "Select",
                options: [{
                    label: "普通options",
                    value: ""
                }, {
                    label: "带新增按钮",
                    value: "WithAddBtn"
                }]
            }
        }, */ {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "clearable",
          label: "清除按钮",
          type: "Switch",
          default: false,
        },
      },
    ],
    rules: [],
    events: [
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "change",
          label: "change",
          type: "Adv-ButtonEmitCodeEditor",
        },
      },
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "visible-change",
          label: "visible-change",
          type: "Adv-ButtonEmitCodeEditor",
        },
      },
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "remove-tag",
          label: "remove-tag",
          type: "Adv-ButtonEmitCodeEditor",
        },
      },
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "clear",
          label: "clear",
          type: "Adv-ButtonEmitCodeEditor",
        },
      },
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "blur",
          label: "blur",
          type: "Adv-ButtonEmitCodeEditor",
        },
      },
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "focus",
          label: "focus",
          type: "Adv-ButtonEmitCodeEditor",
        },
      },
    ],
  },
};
