export default {
    "mobile": {
        attrs: [{
            /**
             * 当pathInJson为undefined/"ComponentAttr"时,属性位于metaItem.componentAttr.里面
             * 当pathInJson为""时,属性位于metaItem.里面
             */
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "style",
                label: "容器style",
                type: "Input",
            }
        }, {
            pathInJson: "",
            uiConfigInPanel: {
                key: "default",
                label: "默认值",
                type: "Input",
            }
        }],
        rules: [],
        events: []
    },
    "pc": {
        attrs: [{
            /**
             * 当pathInJson为undefined/"ComponentAttr"时,属性位于metaItem.componentAttr.里面
             * 当pathInJson为""时,属性位于metaItem.里面
             * 当pathInJson为"propOptions"时,属性位于propOptions  里面
             */
            pathInJson: "propOptions",
            uiConfigInPanel: {
                key: "isDefaultShow",
                label: "是否默认显示",
                type: "Switch",
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "style",
                label: "容器style",
                type: "Input",
            }
        }, {
            pathInJson: "",
            uiConfigInPanel: {
                key: "default",
                label: "默认值",
                type: "Input",
            }
        }, {
            pathInJson: "",
            uiConfigInPanel: {
                key: "formatterFunc",
                label: "格式化值",
                type: "Adv-ButtonEmitCodeEditor",
                default: "",
                componentAttr: {
                    lang: "javascript",
                    textWrapperConfig: {
                        isRemoveWhenSubmit: true,
                        prefixText: `/\*\nconfigObj={rowIdx,colIdx,metaKey,metaItem,metaItemList,rowData,formData,tableBodyData}\n*/\nfunction (configObj){`,
                        suffixText: "}"
                    }
                }
            }
        }, {
            pathInJson: "",
            uiConfigInPanel: {
                key: "isMappingToOptionsLabel",
                label: "是否映射类型",
                type: "Switch",
                default: false,
                events: (configObj) => {
                    return {
                        change: (val) => {
                            const matchedMeta = configObj.metaItemList.find(el => el.key === "optionsConfig")
                            matchedMeta.isShow = val
                        }
                    }
                }
            }
        }, {
            pathInJson: "dynmicConfig",
            uiConfigInPanel: {
                key: "optionsConfig",
                label: "映射选项",
                type: "Adv-ButtonEmitCodeEditor",
                isShow: false,
                componentAttr: {
                    lang: "json",
                    pickedDefaultCodeAdviceVal: "DictData",
                    codeAdviceOptions: [{
                        label: "来自数据库字典",
                        value: "DictData",
                        data: {
                            "dataFromType": "DictData",
                            "params": {
                                "dictTypeCode": "resource_type"
                            },
                            "labelKeyAlias": "name",
                            "valueKeyAlias": "value"
                        }
                    }, {
                        label: "来自静态文本",
                        value: "Static",
                        data: {
                            "dataFromType": "Static",
                            "options": [{
                                "label": "选项a",
                                "value": "a"
                            }, {
                                "label": "选项b",
                                "value": "b"
                            }]
                        }
                    }, {
                        label: "来自getList接口",
                        value: "VmCode",
                        data: {
                            "dataFromType": "VmCode",
                            "vmCode": "Resource",
                            "params": {},
                            "labelKeyAlias": "name",
                            "valueKeyAlias": "id"
                        }
                    }, {
                        label: "来自Api接口",
                        value: "Api",
                        data: {
                            "dataFromType": "Api"
                        }
                    }]
                },
            }
        },],
        rules: [],
        events: []
    }
}