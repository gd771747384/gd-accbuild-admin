export default (props) => {
  //如果动态函数中存在 行 数据时;就表格每行都会触发
  const isRowIndependent = computed(() => {
    if (
      typeof props.metaDynmicConfig === "object" &&
      typeof props.metaDynmicConfig.onChangeFunc === "string"
    ) {
      const isRowIndependent = ["rowIdx", "rowData"].some((el) =>
        props.metaDynmicConfig.onChangeFunc.indexOf(el)
      );
      return isRowIndependent;
    } else {
      return false;
    }
  });
};

export const onChangeFunc = ({ value, props }) => {
  if (
    typeof props.metaDynmicConfig === "object" &&
    typeof props.metaDynmicConfig.onChangeFunc === "string"
  ) {
    const getDynmicFunc = new Function(
      "configObj",
      props.metaDynmicConfig.onChangeFunc
    );
    getDynmicFunc({
      metaItem: props.metaItem,
      metaItemList: props.metaItemList,
      commonAttr: props.commonAttr,
      formData: props.formData,
      rowIdx: props.rowIdx,
      colIdx: props.colIdx,
      rowData: props.rowData,
      tableBodyData: props.tableBodyData,
      value,
    });
  }
};

// 柯里化为了保证事件入参定义时已确定
export const allOnEventFuncs = {
  change: ({ props }) => {
    return (value) => onChangeFunc({ value, props });
  },
};
