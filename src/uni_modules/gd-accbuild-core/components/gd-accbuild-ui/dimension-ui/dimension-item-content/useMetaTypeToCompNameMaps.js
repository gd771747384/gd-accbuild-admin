
/* adv-ui */
import AdvButtonEmitCodeEditor from "./components/adv-components/button-emit-code-editor/button-emit-code-editor.vue";
import AdvInputCaptcha from "./components/adv-components/input-captcha/input-captcha.vue";
import AdvInputGenerateCode from "./components/adv-components/input-generate-code/input-generate-code.vue";
import AdvStrinputWithJsondialog from "./components/adv-components/strinput-with-jsondialog/strinput-with-jsondialog.vue";


/* gd-ui */
import GdAvatar from "./components/gd-components/avatar/avatar.vue";
import GdButton from "./components/gd-components/button/button.vue";
import GdCascader from "./components/gd-components/cascader/cascader.vue";
import GdCheckbox from "./components/gd-components/checkbox/checkbox.vue";
import GdCustomize from "./components/gd-components/customize/customize.vue";
import GdDateTimePicker from "./components/gd-components/date-time-picker/date-time-picker.vue";
import GdIcon from "./components/gd-components/icon/icon.vue";
import GdIconPicker from "./components/gd-components/icon-picker/icon-picker.vue";
import GdInput from "./components/gd-components/input/input.vue";
import GdRadio from "./components/gd-components/radio/radio.vue";
import GdRate from "./components/gd-components/rate/rate.vue";
import GdRichText from "./components/gd-components/rich-text/rich-text.vue";
import GdSelect from "./components/gd-components/select/select.vue";
import GdSerialNo from "./components/gd-components/serial-no/serial-no.vue";
import GdSlider from "./components/gd-components/slider/slider.vue";
import GdSwitch from "./components/gd-components/switch/switch.vue";
import GdTag from "./components/gd-components/tag/tag.vue";
import GdText from "./components/gd-components/text/text.vue";



export default {
    "Adv-ButtonEmitCodeEditor": AdvButtonEmitCodeEditor,
    "Adv-InputCaptcha": AdvInputCaptcha,
    "Adv-InputGenerateCode": AdvInputGenerateCode,
    "Adv-StrinputWithJsondialog": AdvStrinputWithJsondialog,
    Avatar: GdAvatar,
    Button: GdButton,
    Cascader: GdCascader,
    Checkbox: GdCheckbox,
    Customize: GdCustomize,
    DateTimePicker: GdDateTimePicker,
    Icon: GdIcon,
    IconPicker: GdIconPicker,
    Input: GdInput,
    Radio: GdRadio,
    Rate: GdRate,
    RichText: GdRichText,
    Select: GdSelect,
    SerialNo: GdSerialNo,
    Slider: GdSlider,
    Switch: GdSwitch,
    Tag: GdTag,
    Text: GdText,

}
