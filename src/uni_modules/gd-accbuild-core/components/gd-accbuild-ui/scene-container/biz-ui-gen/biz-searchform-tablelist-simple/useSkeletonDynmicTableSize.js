import { reactive,toRefs } from "vue"
import useDynmicTableSize from "@gd-accbuild-ui/scene-container/scene-container-table-form/vxe/hooks/useDynmicTableSize"
export default (props) => {
    const { computedTableFormContainerHeight,cutPageTagsContainerHeight,cutDefaultContainerGutter } = useDynmicTableSize(props)
    return reactive({ tableSkeletonHeight: computedTableFormContainerHeight,toolbarSkeletonHeight:cutPageTagsContainerHeight,defaultSkeletonMargin:cutDefaultContainerGutter })
}