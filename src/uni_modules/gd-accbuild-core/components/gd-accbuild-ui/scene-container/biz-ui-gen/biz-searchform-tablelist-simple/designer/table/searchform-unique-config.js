export default {
    "mobile": {
        attrs: [{
            /**
             * 当pathInJson为undefined/"ComponentAttr"时,属性位于metaItem.componentAttr.里面
             * 当pathInJson为""时,属性位于metaItem.里面
             */
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "style",
                label: "容器style",
                type: "Input",
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "type",
                label: "显示类型",
                type: "Select",
                default: "text",
                options: [{
                    label: "text",
                    value: "text"
                }, {
                    label: "textarea",
                    value: "textarea"
                }, {
                    label: "password",
                    value: "password"
                }]
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "placeholder",
                label: "placeholder",
                type: "Input",
            }
        }, {
            pathInJson: "",
            uiConfigInPanel: {
                key: "default",
                label: "默认值",
                type: "Input",
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "size",
                label: "尺寸",
                type: "Select",
                default: "default",
                options: [{
                    label: "large",
                    value: "large"
                }, {
                    label: "default",
                    value: "default"
                }, {
                    label: "small",
                    value: "small"
                }]
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "autosize",
                label: "autosize",
                type: "Switch",
                default: false
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "maxlength",
                label: "maxlength",
                type: "Input",
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "readonly",
                label: "readonly",
                type: "Switch",
                default: false
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "disabled",
                label: "disabled",
                type: "Switch",
                default: false
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "showWordLimit",
                label: "统计字数提示",
                type: "Switch",
                default: false
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "clearable",
                label: "清除按钮",
                type: "Switch",
                default: false
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "showPassword",
                label: "密码显隐按钮",
                type: "Switch",
                default: false
            }
        }],
        rules: [],
        events: [{
            pathInJson: "events",
            uiConfigInPanel: {
                key: "blur",
                label: "blur",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "focus",
                label: "focus",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "change",
                label: "change",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "input",
                label: "input",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "clear",
                label: "clear",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }]
    },
    "pc": {
        attrs: [{
            /**
             * 在Resource的uiOptions里面
             */
            pathInJson: "pageUiTemplateConfigs.$.templateProps.tableCommonAttr",
            uiConfigInPanel: {
                key: "containerStyle",
                label: "表单容器风格",
                type: "Select",
                options: [{
                    label: "下拉展开(表格正上方)",
                    value: "Inline"
                }, {
                    label: "下拉展开(toolbar右侧)",
                    value: "SingleLine"
                }, {
                    label: "抽屉",
                    value: "Drawer"
                }, {
                    label: "弹窗",
                    value: "Dialog"
                }],
            },
            belongToTemplateCompNames: ["searchform"]
        }, {
            pathInJson: "pageUiTemplateConfigs.$.templateProps.tableCommonAttr.treeTableConfig",
            uiConfigInPanel: {
                key: "readonly",
                label: "是否全局只读",
                type: "Switch",
                default: false
            }
        }, {
            pathInJson: "pageUiTemplateConfigs.$.templateProps.tableCommonAttr",
            uiConfigInPanel: {
                key: "disable",
                label: "是否全局禁用",
                type: "Switch",
                default: false,
            }
        }],
        rules: [],
        events: [/* {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "submitFunc",
                label: "提交表单事件",
                type: "Adv-ButtonEmitCodeEditor",
            },
            belongToTemplateCompNames: ["btnsform"]
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "onFormValidateFunc",
                label: "表单触发校验",
                type: "Adv-ButtonEmitCodeEditor",
            },
            belongToTemplateCompNames: ["btnsform"]
        }, */ {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "onSearchFunc",
                label: "点击搜索按钮",
                type: "Adv-ButtonEmitCodeEditor",
            },
            belongToTemplateCompNames: ["searchform"]
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "onResetFormFunc",
                label: "点击重置按钮",
                type: "Adv-ButtonEmitCodeEditor",
            },
            belongToTemplateCompNames: ["searchform"]
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "onMoreFormFunc",
                label: "点击更多按钮",
                type: "Adv-ButtonEmitCodeEditor",
            },
            belongToTemplateCompNames: ["searchform"]
        }, /* {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "onWatchFormFunc",
                label: "表单数据改变",
                type: "Adv-ButtonEmitCodeEditor",
            }
        } */]
    }
}