
/**
 * 获取表头宽度
 */
export const useGetTableColumnWidth = (curMetaData,tableBodyData) => {
    return getHeaderWidthAlgorithm(curMetaData,tableBodyData)
}


/**
 * 获取表头宽度算法
 */
const getHeaderWidthAlgorithm = (curMetaData,tableBodyData,getTreeTableBodyDataMaxDepth=1) => {
    const appendExpandIconWidth = 0;
    let width = 0;
    if (curMetaData.key !== "buildInSeq") {
        // 计算表格cell的宽度
        const needAppendComponentWidth = ["Input", "Select"].includes(
            curMetaData.type
        );
        let matchBodyWidth = getMinWidthByMatchLenStr(curMetaData,tableBodyData);
        needAppendComponentWidth
            ? (matchBodyWidth = matchBodyWidth + 50)
            : null;

        // 计算header的宽度
        let headerWidth = getMinWidthByHeaderStr(
            curMetaData?.label ?? curMetaData.key
        );

        // 排序图标追加的宽度
        if (curMetaData.sortable !== undefined) {
            const sortablePlaceolderWidth = 30;
            headerWidth += sortablePlaceolderWidth;
        }
        
        // 表格不是很宽的情况下,哪个大用哪个以便显示全，如果太大了,就用header的
        const thresholdTopLimit = 200; // 阈值上限
        width =
            matchBodyWidth < thresholdTopLimit
                ? Math.max(matchBodyWidth, headerWidth)
                : thresholdTopLimit - 30; // headerWidth;
    } else {
        if (getTreeTableBodyDataMaxDepth === 1) {
            width = 35;
            curMetaData.width = width
        } else {
            width = getTreeTableBodyDataMaxDepth * 8.4;
        }
    }

    return width + appendExpandIconWidth;
}

/**
 * 获取表格数据中最长的字符串的width
 */
const getMinWidthByMatchLenStr = (curMetaData, tableBodyData) => {
    const matchLenStrArr = tableBodyData
        .map(el => el[curMetaData.key])
        .filter(el => typeof el === "string");
    let maxLenStr =
        matchLenStrArr.length > 0
            ? matchLenStrArr.reduce((a, b) => (a.length > b.length ? a : b))
            : curMetaData.key;
    const appendLenStr = ["Input", "Select"].includes(curMetaData.type)
        ? "a".repeat(130)
        : "";
    if (curMetaData.type === "Select") {
        let maxOptionStr = maxLenStr;
        if (
            Array.isArray(curMetaData?.options) &&
            curMetaData.options.length > 1
        ) {
            maxOptionStr = curMetaData.options
                .map(el => el.label)
                .reduce((a, b) => (a.length > b.length ? a : b));
        } else if (
            typeof curMetaData?.options === "object" &&
            Array.isArray(curMetaData?.options?.defaultValue) &&
            curMetaData.options.defaultValue.length > 1
        ) {
            const allRowOptionsLabel = [];
            Object.keys(curMetaData.options).forEach(key => {
                const curRowLabels = curMetaData.options[key].map(el => el.label);
                allRowOptionsLabel.push(...curRowLabels);
            });
            maxOptionStr = allRowOptionsLabel.reduce((a, b) =>
                a.length > b.length ? a : b
            );
        }
        maxLenStr =
            maxOptionStr.length > maxLenStr.length ? maxOptionStr : maxLenStr;
    }
    // console.log(maxLenStr);
    return getMinWidthByHeaderStr(maxLenStr + appendLenStr);
}

/**
   * 根据表头字符串 获取宽度
   */
const getMinWidthByHeaderStr = (str) => {
    const len = countChineseEnPlaceholder(str);
    const headerWidth = getMinWidthByFormula(len);
    return headerWidth;
}


/**
 * 根据长度按公式获取宽度
 */
const getMinWidthByFormula = (len) => {
    const fontSize = 6.8;
    const baseLen = 5; // 加上一个文字长度
    return fontSize * (len + baseLen);
}

/**
 * 计算中英文的所占空间;解决 中文比英文更宽，导致表头太空
 * 用于 rewriteHeader 函数
 */
const countChineseEnPlaceholder = (str) => {
    const chineseReg = new RegExp("[\\u4E00-\\u9FFF]+", "g");
    let chineseCount = 0;
    str &&
        str.split("").forEach(el => {
            if (chineseReg.test(el)) {
                chineseCount++;
            }
        });
    const strLen = str?.length ?? 10;
    const l = strLen - chineseCount + chineseCount * 2.7;
    return l;
}