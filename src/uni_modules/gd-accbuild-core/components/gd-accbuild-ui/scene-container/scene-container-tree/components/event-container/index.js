import { useComputedMergeAllBtnsConfig } from "@gd-accbuild-ui/hooks/useComputedMergeAllBtnsConfig"



export const getToolBarConfig = (treeBtnsConfigRefProxy, emits) => {
    const onClick = (params) => {
        emits("do-operation", { eventFrom: "treeHeader", ...params });
    };
    const buildInBtnsDefaultConfig = {
        buildInAddBtn: {
            key: "buildInAddBtn",
            label: "添加一级节点",
            isShow: true,
            onClick: onClick,
            componentAttr: {
                type: "primary",
                icon: {
                    site: "suffix",
                    name: "plus",
                },
            },
        },
    };
    return useComputedMergeAllBtnsConfig(treeBtnsConfigRefProxy, { buildInBtnsDefaultConfig, emits });
}

export const getOperationBtnsConfig = (treeBtnsConfigRefProxy, emits) => {
    console.log(treeBtnsConfigRefProxy,'操作按钮')
    const onClick = (params) => {
        emits("do-operation", { eventFrom: "treeItem", ...params });
    };
    const buildInBtnsDefaultConfig = {
        buildInAddChildIntoCurBtn: {
            key: "buildInAddChildIntoCurBtn",
            label: "添加子节点",
            isShow: true,
            onClick: onClick,
            componentAttr: {
                type: "primary",
                icon: {
                    site: "suffix",
                    name: "plus",
                },
            },
        },
        buildInEditBtn: {
            key: "buildInEditBtn",
            label: "编辑",
            isShow: true,
            onClick: onClick,
            componentAttr: {
                type: "primary",
                icon: {
                    site: "suffix",
                    name: "edit",
                },
            },
        },
        buildInRowDeleteBtn: {
            key: "buildInRowDeleteBtn",
            label: "删除",
            isShow: true,
            onClick: onClick,
            componentAttr: {
                type: "warning",
                icon: {
                    site: "suffix",
                    name: "delete",
                },
            },
            containerAttr: {
                containerStyle: "Tip",
                tipMesssage: "是否执行此操作!",
                buttons: [{
                    key: "buildInContainerSubmitBtn",
                    label: "执行",
                    containerAttr: {
                        containerStyle: "Tip",
                        title: "",
                        //operationType: "Add",
                    },
                },],
            },
        },
    };
    return useComputedMergeAllBtnsConfig(treeBtnsConfigRefProxy, { buildInBtnsDefaultConfig, emits });
}