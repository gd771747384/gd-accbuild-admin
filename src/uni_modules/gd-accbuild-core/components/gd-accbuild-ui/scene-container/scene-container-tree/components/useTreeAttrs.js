import { computed, ref, watch } from "vue";
import pcadmin_globalStyles from "@gd-accbuild-core/theme-chalk/pc-admin/js.module.scss";
/**
 * tree相关属性定义
 */
export default (props) => {
  const tableCommonAttrDefault = {
    size: "small", //"default",
    treeTitle: "标题",
    nodeKey: "id",
    parentNodeKey: "parentId",
    labelField: "id",
    treeSelectionType: "none", // ["none", "Multi", "Single"]
    lazy: true,
    appendFieldsIntoSearchForm: [],
    fillFieldsIntoAddForm: [],
    treeNodeConfig: {},
    searchConfig: {
      hasSearch: true,
    },
    headerConfig: {
      hasHeader: true,
      title: "标题",
    },
  };
  const nodeKey = computed(() => {
    return props.tableCommonAttr?.nodeKey ?? tableCommonAttrDefault.nodeKey;
  });
  const parentNodeKey = computed(() => {
    return (
      props.tableCommonAttr?.parentNodeKey ?? tableCommonAttrDefault.parentNodeKey
    );
  });
  const lazy = computed(() => {
    console.log(props.tableCommonAttr?.lazy ?? tableCommonAttrDefault.lazy,'是否懒加载')
    return props.tableCommonAttr?.lazy ?? tableCommonAttrDefault.lazy;
  });
  const treeNodeConfig = computed(() => {
    return (
      props.tableCommonAttr?.treeNodeConfig ??
      tableCommonAttrDefault.treeNodeConfig
    );
  });
  const loadVmCode = computed(() => {
    const treeAddBtnConfig = props.toolBarConfig.buttons.find(
      (btnConfig) => btnConfig.key === "buildInAddBtn"
    );
    return treeAddBtnConfig.vmCode;
  });
  const operationBtnsConfig = computed(() => {
    return props.tableCommonAttr?.operationBtnsConfig ?? props.operationBtnsConfig;
  });
  const hasSearch = computed(() => {
    return props.tableCommonAttr?.searchConfig?.hasSearch ?? true;
  });
  const hasHeader = computed(() => {
    return props.tableCommonAttr?.headerConfig?.hasHeader ?? true;
  });
  const toolBarConfig = computed(() => {
    return props.toolBarConfig;
  });
  const treeHeaderContainerHeight = computed(() => {
    return (
      props.tableCommonAttr?.headerConfig?.height ??
      pcadmin_globalStyles.accbuildToolbarHeightDefault
    );
  });
  const treeContentHeight = computed(() => {
    return (
      props.tableCommonAttr?.treeContentHeight ??
      `calc(
      100% - ${pcadmin_globalStyles.accbuildToolbarHeightDefault} - #{$vxe-table-row-height-default}
    )`
    );
  });
  const treeProps = computed(() => {
    return {
      children: "children",
      label:
        props.tableCommonAttr?.labelField ?? tableCommonAttrDefault.labelField,
      isLeaf: "_isLeaf",
    };
  });
  const treeSearchFormData = ref({ keyword: "" });
  const unWatchInitTreeSearchForm = watch(
    () => props.treeSearchFormItemMetaList,
    (newVal, oldVal) => {
      treeSearchFormData.value =
        newVal.length >= 1 ? { [newVal[0].key]: "" } : { keyword: "" };
      unWatchInitTreeSearchForm();
    },
    { deep: true }
  );
  const searchFormInputField = computed(() => {
    return props.treeSearchFormItemMetaList.length >= 1
      ? props.treeSearchFormItemMetaList[0].key
      : "keyword";
  });
  return {
    tableCommonAttrDefault,
    nodeKey,
    parentNodeKey,
    lazy,
    treeNodeConfig,
    loadVmCode,
    operationBtnsConfig,
    hasSearch,
    hasHeader,
    toolBarConfig,
    treeHeaderContainerHeight,
    treeSearchFormData,
    searchFormInputField,
    treeContentHeight,
    treeProps,
  };
};
