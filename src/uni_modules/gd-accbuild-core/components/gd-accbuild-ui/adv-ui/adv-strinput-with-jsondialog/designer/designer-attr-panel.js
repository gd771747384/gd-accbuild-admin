export default {
    "mobile": {
        attrs: [],
        rules: [],
        events: []
    },
    "pc": {
        attrs: [{
            /**
             * 当pathInJson为undefined/"ComponentAttr"时,属性位于metaItem.componentAttr.里面
             * 当pathInJson为""时,属性位于metaItem.里面
             * 当pathInJson为"propOptions"时,属性位于propOptions  里面
             */
            pathInJson: "propOptions",
            uiConfigInPanel: {
                key: "isDefaultShow",
                label: "是否默认显示",
                type: "Switch",
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "style",
                label: "容器style",
                type: "Adv-ButtonEmitCodeEditor",
                default: "{}",
                componentAttr: {
                    lang: "json"
                }
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "metaItemList",
                label: "表单项的元数据",
                type: "Adv-ButtonEmitCodeEditor",
                default: "[]",
                componentAttr: {
                    lang: "json"
                }
            }
        },{
            pathInJson:undefined,
            uiConfigInPanel:{
                key:"disabledContentType",
                label: "禁用内容类型",
                type:"Switch",
                default:false
            }
        }],
        rules: [],
        events: []
    }
}