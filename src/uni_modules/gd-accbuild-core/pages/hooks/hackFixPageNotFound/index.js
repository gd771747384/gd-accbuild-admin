import {
  globalConfig,
  resourceApiAliasConfig,
} from "@gd-accbuild-core/config";
import { getUrlPath } from "@gd-accbuild-core/config/utils";
import { getRouterInfo, jumpTo } from "../hackVueRouter";
/**
 * @typedef {Object} HackFixPageNotFoundParams
 * @property {Object} store - store
 * @property {Object} curPageInfo - curPageInfo
 * @property {boolean} isJumpFromAddrInput - 是否地址栏直接输入
 */
/**
 * @description: 页面not found拦截
 * @param {HackFixPageNotFoundParams} Params
 * @return {void}
 */
export const hackFixPageNotFound = async ({
  store,
  curPageInfo,
  isJumpFromAddrInput,
}) => {
  const uiOptionsKeyAlias = resourceApiAliasConfig["uiOptionsKeyAlias"];
  const pageUiTemplateConfigsKeyAlias =
    resourceApiAliasConfig["pageUiTemplateConfigsKeyAlias"];
  /////////////////////
  let validTemplateCompPage = true;
  //const routerInfo = getRouterInfo();
  let curAddressPath = "";
  if (isJumpFromAddrInput) {
    curAddressPath = getUrlPath();
  } else {
    curAddressPath = curPageInfo.targetResourceUrl;
  }
  //const curAddressPath = routerInfo.path;
  let isFirstLoadResource = false;
  if (curAddressPath === curPageInfo?.targetResourceUrl) {
    console.log(1111);
    validTemplateCompPage = Array.isArray(
      curPageInfo?.resource?.[uiOptionsKeyAlias]?.[
        pageUiTemplateConfigsKeyAlias
      ]
    );
  } else if (!curPageInfo?.targetResourceUrl || curAddressPath) {
    console.log(2222);
    if (store.state.permission.pages.length === 0) {
      const resUserResource = await globalConfig.getUserResource();
      await store.dispatch("permission/generateRoutes", resUserResource);
      isFirstLoadResource = true;
    }
    curPageInfo = store.state.permission.pages.find(
      (page) => page.targetResourceUrl === curAddressPath
    );
    store.commit("permission/SET_CURPAGEINFO", curPageInfo);
  }
  /*  &&
    curPageInfo.resource[uiOptionsKeyAlias][pageUiTemplateConfigsKeyAlias].length === 1; */
  validTemplateCompPage =
    curPageInfo &&
    Array.isArray(
      curPageInfo?.resource?.[uiOptionsKeyAlias]?.[
        pageUiTemplateConfigsKeyAlias
      ]
    );
  if (validTemplateCompPage) {
    console.log("hhh");
    /* const templateName =
      curPageInfo.resource[uiOptionsKeyAlias][pageUiTemplateConfigsKeyAlias][0].templateName;
      transName(
        templateName,
        "Pascal",
        "Kebab"
      ) */

    const newUrl = `/uni_modules/gd-accbuild-core/pages/cross-platform/default-crud-pages/index`;
    jumpTo({
      url: newUrl,
      isFirstLoadResource,
    });
  } else {
    const newUrl = `/uni_modules/gd-accbuild-core/pages/cross-platform/default-err-pages/404/index`;
    /* javascript-obfuscator:disable */
    uni.redirectTo({ url: newUrl });
    /* javascript-obfuscator:enable */
  }
};

export const hackFixH5AdressBar = (store) => {
  if (store.state.permission?.curPageInfo?.targetResourceUrl) {
    let replaceURL = `${
      globalConfig.ROUTER_MODE === "hash"
        ? "#"
        : globalConfig.BASE_ROUTER_PREFIX_PATH
    }${store.state.permission.curPageInfo.targetResourceUrl}`;
    history.replaceState({}, "", replaceURL);
    /* javascript-obfuscator:disable */
    uni.setNavigationBarTitle({
      title: store.state.permission.curPageInfo.name,
    });
    /* javascript-obfuscator:enable */
  }
};
