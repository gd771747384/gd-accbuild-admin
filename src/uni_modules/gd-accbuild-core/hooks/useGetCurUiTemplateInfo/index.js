import {
  getPageUiTemplateConfigs,
  globalConfig,
} from "@gd-accbuild-core/config";
import { getUrlPath } from "@gd-accbuild-core/config/utils";
import getMetaListAndData from "./utils/getMetaListAndData";

import getPageUiTemplateMetaListAndData from "./utils/getPageUiTemplateMetaListAndData";
export { getCurEventDynmicMetaAttrSettings } from "./utils/mergeMetaFromColumn";
/**
 * 获取当前ui模板状态;use函数,只允许在页面级别的setup中调用
 * @param {Object} templateConfig
 * @param {String} templateConfig.templateName 对应页面uiOption中匹配的模板名
 * @param {Number} templateConfig.templateIdx 对应页面uiOption中匹配的模板idx 如果为数字,则说明只有一个模板数据
 * @param {String} [templateConfig.activedVmCodeInGroup=null] 多标签切换的vmCode
 * @param {String} [templateConfig.templateInnerSearchPath=null] 当前匹配模板的内部的搜索路径 必须与 templateIdx 同时使用;即单模板才能使用
 * @param {Object} [templateConfig.store=null] 如果参数中有store则不用再调用 useInitPageStore
 */
export default async ({
  templateName,
  templateIdx,
  activedVmCodeInGroup = null,
  templateInnerSearchPath = null,
  store = null,
  isUseStaticResource = false,
  staticResource = {},
  appendReqParams = {},
  allOverrideParams = [],
}) => {
  if (!store) {
    store = globalConfig.store;
  }
  //说明: Menu级别的页面列表的 column 元数据 来自资源接口; Page级别的页面的页面列表的 column 元数据 来自对应页面的entity接口
  //Menu级别的页面只有移动端才有,就是底下的tabbar

  let curPageResource = isUseStaticResource
    ? staticResource
    : store.state.permission.curPageInfo.resource;
  if (!curPageResource && !isUseStaticResource) {
    if (globalConfig.targetPlatform === "pc-admin") {
      const curUrl = getUrlPath();
      //const curUrl = window.location.href.split("#")[1] === "" ? "/" : window.location.href.split("#")[1]
      const matchedPageInfo = store.state.permission.pages.find(
        (el) => el.targetResourceUrl === curUrl
      );
      curPageResource = matchedPageInfo;
    }
    if (!curPageResource) {
      return null;
    }
  }
  const curPageUiTemplateConfigs = getPageUiTemplateConfigs(curPageResource);
  let curPageUiTemplateMetaList = {};
  let curPageUiTemplateData = {};
  let curPageUiTemplateMetaListGroup = {}; //当多标签时,非懒加载方式,暂不使用
  let curPageUiTemplateDataGroup = {};
  let curPageUiTemplateConfig = {}; //仅在templeteIdx存在时有值
  //console.log(curPageUiTemplateConfigs, templateIdx,templateName, "======");
  const curComponentTempleteConfig = curPageUiTemplateConfigs.find(
    (templateConfig, idx) =>
      templateConfig.templateName === templateName && idx === templateIdx
  );
  //console.log(curComponentTempleteConfig,'匹配的模板配置')
  if (!curComponentTempleteConfig) {
    console.error("没找到对应模板");
  } else if (
    curComponentTempleteConfig.dataFromType === "VmCodeConfigGroup" &&
    !activedVmCodeInGroup
  ) {
    curPageUiTemplateConfig = curComponentTempleteConfig;
  } else {
    let overrideParams = {};
    if (Array.isArray(allOverrideParams)) {
      const e = allOverrideParams.find(
        (el) => el.templateInnerSearchPath === templateInnerSearchPath
      );
      if (e) {
        overrideParams = e;
      }
    }
    //templateInnerSearchPath
    const res = await getPageUiTemplateMetaListAndData({
      resource: curPageResource,
      getMetaListAndDataCallback: getMetaListAndData,
      templateIdx: templateIdx,
      store: store,
      activedVmCodeInGroup: activedVmCodeInGroup,
      templateInnerSearchPath,
      appendReqParams,
      overrideParams,
      isUseStaticResource,
      staticResource,
    });
    if (
      typeof res === "object" &&
      res.hasOwnProperty("vaildFailureTemplateInnerSearchPath")
    ) {
      return Promise.resolve({
        vaildFailureTemplateInnerSearchPath:
          res.vaildFailureTemplateInnerSearchPath,
        store,
      });
    }
    curPageUiTemplateConfig = res.curPageUiTemplateConfig;
    if (res.allTemplateMetaList.length === 1) {
      curPageUiTemplateMetaList = res.allTemplateMetaList[0];
      curPageUiTemplateData = res.allTemplateData[0];
    } else {
      res.allTemplateData.forEach((el, idx) => {
        curPageUiTemplateMetaListGroup[el.vmCode] =
          res.allTemplateMetaList[idx];
        curPageUiTemplateDataGroup[el.vmCode] = res.allTemplateData[idx];
      });
    }
  }

  return Promise.resolve({
    curPageUiTemplateMetaList,
    curPageUiTemplateData,
    curPageUiTemplateConfig,
    ///////////
    curPageUiTemplateMetaListGroup,
    curPageUiTemplateDataGroup,
    store,
  });
};
