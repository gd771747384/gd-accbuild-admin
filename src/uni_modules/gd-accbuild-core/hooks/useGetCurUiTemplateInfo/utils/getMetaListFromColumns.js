import { transName, mapKeysToTarget } from "@gd-accbuild-core/config/utils";
import { resourceApiAliasConfig } from "@gd-accbuild-core/config";
const {
  resourceUrlKeyAlias,
  childrenKeyAlias,
  realPageFileKeyAlias,
  resourceTypeKeyAlias,
  resourcePermsKeyAlias,
  resourceNameAlias,
  resourceIconAlias,
  idKeyAlias,
  pidKeyAlias,
  resourcePermsKeyDefinedRule,
  targetKeyAlias,
  hasChildrenAlias,
  ordinalKeyAlias,
  uiOptionsKeyAlias,
  hackSuffixJumpStr,
  getJumpTargetVal,
  getJumpTargetReourcePermsKey,
} = resourceApiAliasConfig;
import mergeMetaFromColumn from "./mergeMetaFromColumn";
/**
 * 请求获取vmCode 的 columns 和 对应的data
 * @param {Array} allVmTemplatesConfig 需要请求的vmTemplateConfig;[{vmCode:"···",params:{}}]
 * @param {Object} customizeMetaConfig 具体页面文件的自定义元数据配置
 * @param {Object} tabObjHolderConfig 用于弹窗表单多标签的
 */
export default async ({
  columns,
  customizeMetaConfig = {},
  tabObjHolderConfig = {},
  store = null,
  roles = [],
  isUseStaticResource,
  staticResource,
}) => {
  const searchFormItemMetaList = [];
  const searchFormItemDynmicConfigList = [];

  const dialogFormItemMetaList = [];
  const dialogFormItemDynmicConfigList = [];
  const customBtnContainerFormItemMetaListObj = {};
  const customBtnContainerFormItemDynmicConfigListObj = {};
  const tableColMetaList = [];
  const tableColumnDynmicConfigList = [];

  const promiseArr = [];
  columns.forEach((column) => {
    promiseArr.push(
      mergeMetaFromColumn({
        column,
        tabObjHolderConfig,
        store,
        roles,
        isUseStaticResource,
        staticResource,
      })
    );
  });
  const allColumnsMeta = await Promise.all(promiseArr);

  allColumnsMeta.forEach((meta, idx) => {
    if (
      columns[idx]["propOptions"].hasOwnProperty("searchformConfig") &&
      columns[idx]["propOptions"]["searchformConfig"].isOutput
    ) {
      searchFormItemMetaList.push(meta.searchFormItemMeta);

      meta.searchFormItemDynmicConfig.key = meta.searchFormItemMeta.key;
      searchFormItemDynmicConfigList.push(meta.searchFormItemDynmicConfig);
    }

    dialogFormItemMetaList.push(meta.dialogFormItemMeta);

    meta.dialogFormItemDynmicConfig.key = meta.dialogFormItemMeta.key;
    dialogFormItemDynmicConfigList.push(meta.dialogFormItemDynmicConfig);

    Object.keys(meta.dialogFormItemDynmicComponentConfig).forEach((btnKey) => {
      if (btnKey.startsWith("custom")) {
        if (!customBtnContainerFormItemMetaListObj.hasOwnProperty(btnKey)) {
          customBtnContainerFormItemMetaListObj[btnKey] = [];
          customBtnContainerFormItemDynmicConfigListObj[btnKey] = [];
        }
        customBtnContainerFormItemMetaListObj[btnKey].push(
          meta.dialogFormItemDynmicComponentConfig[btnKey].staticConfig
        );
        const customeDynmicConfig =
          meta.dialogFormItemDynmicComponentConfig[btnKey]?.dynmicConfig ?? {};
        customeDynmicConfig.key = meta.tableColMeta.key;
        customBtnContainerFormItemDynmicConfigListObj[btnKey].push(
          customeDynmicConfig
        );
      }
    });
    //弹窗表单默认有一个隐藏的主键字段
    if (meta.dialogFormItemMeta.key === idKeyAlias) {
      meta.dialogFormItemMeta.dynmicMetaAttrSettings = {
        isShowWithEventNames: [], // 新增/编辑 弹窗 都看不到这个组件
        canCommitWithEventNames: ["EditRowDialogFormOpen"], // 新增不允许提交id字段、但编辑必须提交id字段
      };
    }

    tableColMetaList.push(meta.tableColMeta);

    meta.tableColumnDynmicConfig.key = meta.tableColMeta.key;
    tableColumnDynmicConfigList.push(meta.tableColumnDynmicConfig);
  });

  return Promise.resolve({
    searchFormItemMetaList,
    dialogFormItemMetaList,
    tableColMetaList,
    searchFormItemDynmicConfigList,
    dialogFormItemDynmicConfigList,
    tableColumnDynmicConfigList,
    ////////////自定义按钮的弹窗表单元数据
    customBtnContainerFormItemMetaListObj,
    customBtnContainerFormItemDynmicConfigListObj,
  });
};
