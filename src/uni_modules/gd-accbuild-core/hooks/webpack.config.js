const path = require('path');
const {CleanWebpackPlugin} = require('clean-webpack-plugin')        
module.exports = {
  entry: {
	  useGetCurUiTemplateInfo: './useGetCurUiTemplateInfo/index.js',
	  useMergeMeta: './useMergeMeta/index.js',
	  buildInBizTemplateProps: './buildInBizTemplateProps/index.js'
  },
  target: "node",
  output: {
    filename: '[name].bundle.js',
	path: __dirname + '/dist'
  },
  mode: "production",
  resolve: {
    extensions: ['.js'],
	alias: {
      "@gd-accbuild-core": path.resolve(__dirname, '../'),
	  "@": path.resolve(__dirname, '../../../')
    },
  },
  module: {
    rules: [
	  /*{
        test: /\.js$/i,
        include: [
          path.resolve(__dirname,'../hooks')
        ],
        exclude: /node_modules/
      },*/
	  {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
      {
        test: /\.s[ac]ss$/i,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
    ]
  },
  plugins: [
        new CleanWebpackPlugin()
    ],
};