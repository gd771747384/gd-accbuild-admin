const AsyncFunction = Object.getPrototypeOf(async function () {}).constructor;
/**
 * 获取解析的函数
 */
export const getFunc = (content) => {
  if (!content) {
    return null;
  } else if (typeof content === "string") {
    const funcAlias = /\bawait\s*.*?\(.*?\)/.test(content)
      ? AsyncFunction
      : Function;
    return new funcAlias("configObj", content);
  } else if (typeof content === "function") {
    return content;
  }
};
/**
 * 动态获取schema的元数据属性;包括执行类的元数据、属性类的元数据
 * @param {Object} args
 * @param {any} args.val 原属性的值 如 isShow的值
 * @param {String | Function} args.valFunc isShowFunc的值
 * @param {String} args.configObj 上面 valtFunc 的接受参数
 */
const getDynmicAttr = ({ val, valFunc, configObj }) => {
  const func = getFunc(valFunc);
  if (!func) {
    return val;
  } else {
    return func(configObj);
  }
};

export default {
  getDynmicAttr,
  getFunc
};
