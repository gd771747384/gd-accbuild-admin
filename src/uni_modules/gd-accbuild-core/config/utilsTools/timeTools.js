/**
 * 判断值具体类型
 * 所有检测类型结果枚举:  "date","regexp","array","object","number","string","null","undefined","function"
 */
const u_getTypeof = (val) => {
    const s = Object.prototype.toString.call(val);
    return s.match(/\[object (.*?)\]/)[1].toLowerCase();
  };

/**
 * 将时间戳 转化为 "n 小时 n 分钟 n 秒" 的形式
 * @param {Object} config
 * @param {number} config.value
 * @param {string | "s" | "m" | "h"} config.fromUnit
 */
const transTimeUnit = ({ value, fromUnit = "s" }) => {
  if (fromUnit === "m") {
    value *= 60;
  } else if (fromUnit === "h") {
    value *= 3600;
  }
  let secondTime = Math.floor(+value); // 秒
  let minuteTime = 0; // 分
  let hourTime = 0; // 小时
  if (secondTime > 60) {
    //如果秒数大于60，将秒数转换成整数
    //获取分钟，除以60取整数，得到整数分钟
    minuteTime = Math.floor(secondTime / 60);
    //获取秒数，秒数取佘，得到整数秒数
    secondTime = Math.floor(secondTime % 60);
    //如果分钟大于60，将分钟转换成小时
    if (minuteTime > 60) {
      //获取小时，获取分钟除以60，得到整数小时
      hourTime = Math.floor(minuteTime / 60);
      //获取小时后取佘的分，获取分钟除以60取佘的分
      minuteTime = Math.floor(minuteTime % 60);
    }
  }
  let time = `${isNaN(secondTime) ? 0 : secondTime}秒`;

  if (minuteTime > 0) {
    time = `${minuteTime}分${time}`;
  }
  if (hourTime > 0) {
    time = `${hourTime}小时${time}`;
  }
  return time;
};
/**
 * 时间戳转换成字符串格式
 * @param {Object} config
 * @param {number | Date} config.timestamp
 * @param {string} config.format 如: "YYYY-MM-DD HH:mm:ss"、"YYYY-MM-DD"
 */
const formatDatetimeFromTimestamp = ({
  timestamp,
  format = "YYYY-MM-DD HH:mm:ss",
}) => {
  let date = null;
  const timestampParamType = u_getTypeof(timestamp);
  if (typeof timestamp === "number" && (timestamp + "").length === 10) {
    date = new Date(timestamp * 1000);
  } else if (timestampParamType === "date") {
    date = timestamp;
  } else if (timestampParamType === "string") {
    date = new Date(timestamp);
  }
  const YYYY = date.getFullYear();
  const MM =
    date.getMonth() + 1 < 10
      ? "0" + (date.getMonth() + 1)
      : date.getMonth() + 1;
  const DD = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
  const hh = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
  const mm =
    date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
  const ss =
    date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
  if (format.length === 19) {
    return `${YYYY}${format[4]}${MM}${format[7]}${DD}${format[11]}${hh}${format[14]}${mm}${format[17]}${ss}`;
  } else if (format.length === 10 || format.length === 11) {
    return `${YYYY}${format[4]}${MM}${format[7]}${DD}`;
  } else if (format.length <= 5) {
    return YYYY;
  } else if (format.length <= 8) {
    return `${YYYY}${format[4]}${MM}`;
  } else if (format.length <= 11) {
    return `${YYYY}${format[4]}${MM}${format[7]}${DD}`;
  } else if (format.length <= 14) {
    return `${YYYY}${format[4]}${MM}${format[7]}${DD}${format[11]}${hh}`;
  } else if (format.length <= 17) {
    return `${YYYY}${format[4]}${MM}${format[7]}${DD}${format[11]}${hh}${format[14]}${mm}`;
  } else {
    return `${YYYY}${format[4]}${MM}${format[7]}${DD}${format[11]}${hh}${format[14]}${mm}${format[17]}${ss}`;
  }
};
/**
 * 校验相关
 */
export default {
  transTimeUnit,
  formatDatetimeFromTimestamp,
};
