## 详细了解

[设计器地址](http://designer.gd-sccbuild.com)<br/>
[文档地址](http://gd771747384.gitee.io/gd-accbuild-doc/zh/guide/start.html)<br/>
[pc-admin工程模板](https://gitee.com/gd771747384/gd-accbuild-admin)<br/>
[纯净版工程模板](https://gitee.com/gd771747384/gd-accbuild-demo-base)<br/>

## 什么是优速搭？

**优速搭**是一款用于搭建 web、app、小程序等应用的低代码平台。而且支持二次开发，允许二次开发后反向可视化继续配置。在不失灵活性的前提下，大大提高您开发应用的效率。<br/>
优速搭包含了 应用设计器、应用市场、模块市场、前端组件库、前端项目框架(uniapp)、后端项目框架(ruoyi-pro)、后端核心包、业务流程引擎、审批流程引擎等。<br/>
优速搭低代码平台由多个功能部分组成，且平台是面向零代码、低代码、高代码所有用户开放的，因此不同用户需要了解的功能有所侧重，以下是不同使用者对应需要侧重了解的功能。

<style>
  .gd-acc--function{
    text-align:center;
    min-width:200px;
  }
  .gd-acc--desc{
    text-align:left;
    min-width:220px;
  }
  .gd-acc--design-for{
    text-align:center;
    min-width:100px;
  }
  .gd-acc--plan{
    text-align:left;
    min-width:150px;
  }
</style>
<div style="overflow-x:scroll;min-width:100%;">
<table>
    <tr>
        <th style="min-width:80px;">分类</th>
        <th style="min-width:130px;">功能部分</th>
        <th>描述</th>
        <th>使用群体</th>
        <th>开发进展与计划</th>
    </tr>
    <tr>
        <th style="min-width:120px;" >优速搭效率工具</th>
        <td class="gd-acc--function">应用设计器</td>
        <td class="gd-acc--desc">可视化的管理数据、配置页面呈现。(支持源码导出)</td>
        <td class="gd-acc--design-for">零代码 NoCode</td>
        <td class="gd-acc--plan">持续完善中并且可以使用</td>
    </tr>
    <tr>
        <th style="min-width:130px;">IDEA插件</th>
        <td class="gd-acc--function">gd-accbuild-idea-extension</td>
        <td class="gd-acc--desc">表结构、UI元数据同步等功能</td>
        <td  class="gd-acc--design-for">低代码 LowCode</td>
        <td class="gd-acc--plan">持续完善中可以使用</td>
    </tr>
    <tr>
        <th style="min-width:120px;" rowspan="4">优速搭核心框架</th>
        <td class="gd-acc--function"><div>前端项目框架</div><div>gd-accbuild-admin</div><div>gd-accbuild-mobile</div></td>
        <td class="gd-acc--desc">允许用户按一定规范修改前端页面代码，并且遵循一定的合并策略，不会在与零代码混合开发时发生冲突。</td>
        <td  class="gd-acc--design-for">低代码 LowCode</td>
        <td class="gd-acc--plan">持续完善中可以使用</td>
    </tr>
    <tr>
        <td class="gd-acc--function"><div>前端组件库</div><div>gd-accbuild-template</div></td>
        <td class="gd-acc--desc">允许用户以 NPM 包的方式使用前端组件。</td>
        <td  class="gd-acc--design-for">高代码 ProCode</td>
        <td class="gd-acc--plan">持续完善中可以使用</td>
    </tr>
    <tr>
        <td class="gd-acc--function"><div>后端项目框架</div><div>不限制</div></td>
        <td class="gd-acc--desc">只需要集成gd-accbuild-server使用即可，默认会根据设计器的配置自动生成CRUD接口。也允许用户根据请求api接口的override实现定制后端逻辑代码，并且不会在与零代码混合开发时发生冲突。</td>
        <td  class="gd-acc--design-for">低代码 LowCode</td>
        <td class="gd-acc--plan">暂未完善,暂不可用。目前使用的是nodejs,计划使用kotlin/js重写,发布成maven包和npm包</td>
    </tr>
    <tr>
        <td class="gd-acc--function">serverless</td>
        <td class="gd-acc--desc">直接使用unCloud部署后端，按量付费价格跟uniCloud标准一致。</td>
        <td  class="gd-acc--design-for">低代码 LowCode</td>
        <td class="gd-acc--plan">持续完善中可以使用</td>
    </tr>
    <tr>
    <th style="min-width:130px;" rowspan="2">优速搭开发者生态</th>
        <td class="gd-acc--function">应用市场</td>
        <td class="gd-acc--desc">由其它用户使用优速搭平台开发的完整应用，能直接复用整个应用，且可在设计器中自己订制。</td>
        <td  class="gd-acc--design-for">零代码 NoCode</td>
        <td class="gd-acc--plan">持续完善中可以使用</td>
    </tr>
    <tr>
        <td class="gd-acc--function">模块市场</td>
        <td class="gd-acc--desc">由其它用户使用优速搭平台开发的模块，能直接复用，且可在设计器中引用集成该模块。</td>
        <td  class="gd-acc--design-for">零代码 NoCode</td>
        <td class="gd-acc--plan">持续完善中可以使用</td>
    </tr>
</table>
</div>

## 宏观设计，三个方面与目前市面上的低代码区别
### 生态架构
```mermaid
flowchart TD
    subgraph 优速搭架构
        gd_org["NPM生态、vue生态"] -->|兼容已有生态| gd["优速搭"]
        style gd fill:#f9f,stroke:#333,stroke-width:4px
        AIGC["AIGC、chatGPT"] -->|辅助| gd_feature2["元数据配置化UI"]
        subgraph gd-accbuild新范式
            gd --> gd_feature2["元数据配置化UI"]
            gd --> gd_feature3["协同开发混合编程"]
            gd --> gd_feature["渐进式"]
            gd --> gd_feature4["支持serverless"]
        end
        subgraph 五大组件赋能
            gd_feature2 --> gd_scene["场景容器"]
            gd_feature2 --> gd_atomic["原子组件"]
            gd_other["..."]
        end
        subgraph gd-accbuild自建生态
            gd_scene --> gd_生态["优速搭组件库生态"]
            gd_atomic --> gd_生态["优速搭组件库生态"]
            gd_生态 --> gd_子生态["scene-container、gd-ui等等"]
        end
        subgraph IDEA插件
            gd_feature2 --> gd_visual["可视化配置Ui元数据工具"]
            gd_feature3 -->|UI元数据双向转化| gd_metaSync["表结构、Ui元数据同步工具"]
            gd_visual --> gd_metaSync
        end
        subgraph 后端生态
            gd_feature4 --> gd_uniCloud["uniCloud"]
            gd_uniCloud <--> gd_ideaData["表结构双向转化"]
            gd_ideaData <--> gd_backend["spring(若依框架)、expressJs"]
            gd_ideaData <--> gd_metaSync
        end
    end
style AIGC fill: #ccf, stroke: #f66, stroke-width: 2px, stroke-dasharray: 5,5;
style gd_生态 fill: #ccf, stroke: #f66, stroke-width: 2px, stroke-dasharray: 5,5;
style gd_子生态 fill: #ccf, stroke: #f66, stroke-width: 2px, stroke-dasharray: 5,5;
style gd_uniCloud fill: #ccf, stroke: #f66, stroke-width: 2px, stroke-dasharray: 5,5;
style gd_backend fill: #ccf, stroke: #f66, stroke-width: 2px, stroke-dasharray: 5,5;
style gd_visual fill: #ccf, stroke: #f66, stroke-width: 2px, stroke-dasharray: 5,5;
style gd_metaSync fill: #ccf, stroke: #f66, stroke-width: 2px, stroke-dasharray: 5,5;
```
## 软件架构

```mermaid
graph TD
subgraph 优速搭框架
    E -...->|逐层组装| X
    X["页面"] --> A["布局组件层(layout-ui)"]
    A -->|提供 布局能力| B("组件模板层")
    B -->|增强 属性和事件| E["原子组件层(atomic-ui)"]
end
subgraph 组件模板层
    m["业务组件(biz-ui)"] -->|注入 meta数据和data数据| C["场景容器(scene-container)"]
    C -->|是维度组件的具体场景封装| D["维度组件(dimension-ui)"]
end
B -...-> m
style A fill: #ccf, stroke: #f66, stroke-width: 2px, stroke-dasharray: 5,5;
style m fill: #ccf, stroke: #f66, stroke-width: 2px, stroke-dasharray: 5,5;
style C fill: #ccf, stroke: #f66, stroke-width: 2px, stroke-dasharray: 5,5;
style D fill: #ccf, stroke: #f66, stroke-width: 2px, stroke-dasharray: 5,5;
style E fill: #ccf, stroke: #f66, stroke-width: 2px, stroke-dasharray: 5,5;
```
大家都知道 渲染 与 出码[参考阿里低代码引擎](https://lowcode-engine.cn/site/docs/guide/create/useRenderer) 方式是低代码业界统一的共识。<br/>
笔者认为渲染模式的潜力肯定大于出码，只要解决schema定义的规范性、组件的自定义、插槽的支持、事件联动的支持等等。因此优速搭软件架构，按照分层设计思想，经过大量实践，在编码体验上、可维护性上、可扩展性上做出了最佳的平衡。

## 范式创新
大多数低代码平台被程序员饱受诟病的是一点是配置化的脚本、在线设计器中东一个西一个很难维护、复杂联动要特别去学习平台的联动语法。这本质上并不是配置化编程的错，因为vue2其实也是一种配置化编程，`data`、`methods`、`watch`什么窟窿填什么内容都约束好了，主要是**它能无需学习成本的支持js生态，渐进式使用**。因此优速搭也是吸收借鉴了前辈的经验。配置规范、提供了[无额外依赖方式引入使用](https://gitee.com/gd771747384/gd-accbuild-demo-base)


## 商业授权

优速搭系列所有代码均免费永久授权用户开发项目，但不得复制抄袭用于低代码相关的与优速搭竞争的平台级产品。
