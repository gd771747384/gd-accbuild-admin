import { defineConfig } from "vite";
import uni from "@dcloudio/vite-plugin-uni";
import VueSetupExtend from "vite-plugin-vue-setup-extend";

import AutoImport from "unplugin-auto-import/vite"; //自动导入
import Components from "unplugin-vue-components/vite"; //自动注册
import { ElementPlusResolver } from "unplugin-vue-components/resolvers";
const IconsResolver = require("unplugin-icons/resolver");
import Icons from "unplugin-icons/vite";

import styleImport from "vite-plugin-style-import";
import vueJsx from "@vitejs/plugin-vue-jsx";

/* import {
	viteCommonjs
} from "@originjs/vite-plugin-commonjs";
    "@originjs/vite-plugin-commonjs": "^1.0.3", */
const path = require("path");
import global from "./src/config/global.js";
//import legacyPlugin from "@vitejs/plugin-legacy";
import esbuild from "rollup-plugin-esbuild";
// https://vitejs.dev/config/
export default defineConfig(({ command, mode }) => {
    let otheradditionalDataStr = ""; //`@import 'tailwindcss/base';@import 'tailwindcss/utilities';`
    let otherPlugins = [];
    if (global.targetPlatform.startsWith("pc")) {
      otheradditionalDataStr =
        otheradditionalDataStr +
        `@import "./src/uni_modules/gd-accbuild-core/theme-chalk/pc-admin/index.module.scss";`;
      otherPlugins = [
        ...otherPlugins,
        AutoImport({
          resolvers: [ElementPlusResolver()],
        }),
        Components({
          resolvers: [
            ElementPlusResolver(),
            IconsResolver({
              prefix: "Icon",
            }),
          ],
          directoryAsNamespace: true,
        }),
        Icons({
          compiler: "vue3",
          autoInstall: true,
        }),
        styleImport({
          libs: [
            {
              libraryName: "../uni_modules/gd-accbuild-core/vxe-table",
              esModule: true,
              resolveComponent: (name) =>
                `../src/uni_modules/gd-accbuild-core/node_modules/vxe-table/es/${name}`,
              resolveStyle: (name) =>
                `../src/uni_modules/gd-accbuild-core/node_modules/vxe-table/es/${name}/style.css`,
            },
          ],
        }),
      ];
    } else if (global.targetPlatform === "mobile") {
      otheradditionalDataStr =
        otheradditionalDataStr +
        `@import "./src/uni_modules/gd-accbuild-core/static/mobile/index.module.scss";`;
      otherPlugins = [
        ...otherPlugins,
        AutoImport({
          resolvers: [ElementPlusResolver()],
        }),
        Components({
          resolvers: [ElementPlusResolver()],
        }),
      ];
    }
    return {
      css: {
        preprocessorOptions: {
          scss: {
            additionalData: otheradditionalDataStr,
          },
        },
        postcss: {
          plugins: [
            {
              postcssPlugin: "internal:charset-removal",
              AtRule: {
                charset: (atRule) => {
                  if (atRule.name === "charset") {
                    atRule.remove();
                  }
                },
              },
            },
          ],
        },
      },
      resolve: {
        alias: {
          "@uni_modules": "/src/uni_modules",
          "@gd-accbuild-core": "/src/uni_modules/gd-accbuild-core",
          "element-plus":
            "/src/uni_modules/gd-accbuild-core/node_modules/element-plus",
          //'@antv/x6-vue-shape': '@antv/x6-vue-shape/lib',
          //'vue': 'vue/dist/vue.runtime.esm-bundler.js',
        },
      },
      plugins: [
        uni(),
        vueJsx(),
        //viteCommonjs(), //解决 vxe-table-plugin-element 的 require()
        ...otherPlugins,
        VueSetupExtend(),
        {
          //ES语法转化 https://www.npmjs.com/package/rollup-plugin-esbuild
          ...esbuild({
            target: "chrome70",
            sourceMap: true, //process.env.NODE_ENV !== 'production', // default
            minify: process.env.NODE_ENV === "production",
            // If necessary, you can add other suffixes in JS TS here.
            include: /\.vue|js|ts|jsx|tsx$/,
            exclude: /node_modules/,
            loaders: {
              // Enable JS in .vue files
              ".vue": "js",
              // Enable JSX in .js files
              ".js": "jsx",
            },
          }),
          enforce: "post",
        },
      ],
      // build: {
      //     target: 'es2015'
      // }
    };
});
