
export const state = {
  device: 'desktop',
  size: 'medium',
  sidebar: {
	  /* javascript-obfuscator:disable */
    opened: uni.getStorageSync('sidebarStatus') ? !!uni.getStorageSync('sidebarStatus') : true,
	  /* javascript-obfuscator:enable */
    withoutAnimation: false
  }
}

export const mutations = {
  TOGGLE_SIDEBAR: (state) => {
    state.sidebar.opened = !state.sidebar.opened
    state.sidebar.withoutAnimation = false
	/* javascript-obfuscator:disable */
    if (state.sidebar.opened) {
        uni.setStorageSync('sidebarStatus', '1')
    } else {
        uni.setStorageSync('sidebarStatus', '')
    }
	/* javascript-obfuscator:enable */
  },
  CLOSE_SIDEBAR: (state, withoutAnimation) => {
    state.sidebar.opened = false
    state.sidebar.withoutAnimation = withoutAnimation
	/* javascript-obfuscator:disable */
    uni.setStorageSync('sidebarStatus', '')
	/* javascript-obfuscator:enable */
  },
  TOGGLE_DEVICE: (state, device) => {
    state.device = device
  },
  SET_SIZE: (state, size) => {
    state.size = size
	/* javascript-obfuscator:disable */
    uni.setStorageSync('size', size)
	/* javascript-obfuscator:enable */
  }
}

export const actions = {
  toggleSideBar({ commit }) {
    commit('TOGGLE_SIDEBAR')
  },
  closeSideBar({ commit }, { withoutAnimation }) {
    commit('CLOSE_SIDEBAR', withoutAnimation)
  },
  toggleDevice({ commit }, device) {
    commit('TOGGLE_DEVICE', device)
  },
  setSize({ commit }, size) {
    commit('SET_SIZE', size)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
