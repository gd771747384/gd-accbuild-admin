
export const state = {
    visitedRoute: [],
    curActivedRoute: null,
    maxVisitedRouteNum: 6,
    noneLeftNavPagePaths: [],
	/* javascript-obfuscator:disable */
    isHashChange:!!uni.getStorageSync('store/historyRoute/isHashChange')//hashchang的标志
	/* javascript-obfuscator:enable */
}

export const mutations = {
    ADD_ROUTE: (state, route) => {
        if (!route?.path) {
            return
        }
        const matchedRouteIdx = state.visitedRoute.findIndex(el => el.path === route.path)
        if (matchedRouteIdx === -1) {
            const orgVisitedRouteLength = state.visitedRoute.length
            if (orgVisitedRouteLength >= state.maxVisitedRouteNum) {
                state.visitedRoute.shift()
            }
            state.visitedRoute.push(route)
            state.curActivedRoute = route
        }
    },
    DEL_ROUTE: (state, route) => {
        const matchedRouteIdx = state.visitedRoute.findIndex(el => el.path === route.path)
        if (matchedRouteIdx === -1) {
            state.visitedRoute.splice(matchedRouteIdx, 1)
            curRouteLength = state.visitedRoute.length
            if (curRouteLength > 0) {
                state.curActivedRoute = state.visitedRoute[curRouteLength - 1]
            } else {
                state.curActivedRoute = null
            }
        }
    },
    SET_VISITEDROUTES: (state, routes) => {
        if (routes.length > 0) {

            state.visitedRoute = routes
        }
    },
    SET_NONELEFTNAVPAGEPATHS: (state, noneLeftNavPagePath) => {
        state.noneLeftNavPagePaths.push(noneLeftNavPagePath)
    },
    SET_ISHASHCHANGE:(state, isHashChange)=>{
        /* javascript-obfuscator:disable */
        uni.getStorageSync('store/historyRoute/isHashChange',isHashChange) 
        /* javascript-obfuscator:enable */
        state.isHashChange = isHashChange
    }
}

export const actions = {
    addRoute({ commit }, route) {
        commit('ADD_ROUTE', route)
    },
    delRoute({ commit }, route) {
        commit('DEL_ROUTE', route)
    },
    setnNoneLeftNavPagePaths({ commit }, noneLeftNavPagePath) {
        commit('SET_NONELEFTNAVPAGEPATHS', noneLeftNavPagePath)
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}
