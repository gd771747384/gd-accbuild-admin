import App from "./App";
import { createSSRApp } from "vue";
import store from "./store";
import router from "./router/index";
import global from "@/config/global";
import { globalHttpApis } from "@/services/sys";
import GdHackDynmicComp from "@/components/util-comp/hack-dynmic-comp";
import { initAccBuildConfig } from "@uni_modules/gd-accbuild-core/config/index";
export function createApp() {
  const app = createSSRApp(App);
  app.use(store);
  initAccBuildConfig({
    app,
    GdHackDynmicComp,
    global: {
      ...global,
      ...globalHttpApis,
      store,
      router,
    },
  });
  return {
    app,
  };
}
