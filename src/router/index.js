import { createRouter } from '@uni_modules/gd-accbuild-core/pages/hooks/hackVueRouter'
import { constantRoutes } from "./constantRoutes"
import { constantResources } from './constantResources'
const router = createRouter({
    constantRoutes,
    constantResources
})
/* router.beforeEach = (routerCallback = (to, from, next) => next()) => {
    console.log('全局导航守卫')
    return routerCallback
} */
export default router