export default [
	{
		"key": "id"
	},
	{
		"key": "moduleCode"
	},
	{
		"key": "version"
	},
	{
		"key": "displayName"
	},
	{
		"key": "descrip"
	},
	{
		"key": "price"
	},
	{
		"key": "isSourceCode"
	},
	{
		"key": "isFreeUpdate"
	},
	{
		"key": "updatePride"
	}
]