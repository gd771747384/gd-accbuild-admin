export default {
	"_id": "62db4c1d0d0822000184bb60",
	"children": [],
	"code": "SysMgr.OwnModule",
	"descrip": null,
	"hasChildren": false,
	"icon": null,
	"id": "0000000000000000008",
	"isEnable": true,
	"isGranted": true,
	"isLeaf": true,
	"isShow": true,
	"moduleCode": "gd_sys",
	"name": "已购模块",
	"ordinal": 40,
	"pagePath": "sys-mgr/own-module",
	"parentId": "0000000000000000001",
	"resourceType": "Page",
	"route": null,
	"targetPlatform": "pc-admin",
	"uiOptions": {
		"layoutStyle": "",
		"pageStyle": "PageUiTemplate",
		"pageUiTemplateConfigs": [
			{
				"templateName": "BizSearchformTablelistSimple",
				"templateProps": {
					"tableCommonAttr": {
						"dataFromType": "VmCode",
						"isEnterReqData": true,
						"moduleCode": "gd_sys",
						"paginationConfig": {
							"isPaged": false,
							"pageObj": {
								"pageNum": 0,
								"pageSize": 0,
								"totalCount": 0
							}
						},
						"size": "default",
						"vmCode": "OwnModule"
					}
				}
			}
		],
		"target": "_tab"
	}
}