export default {
	"buildInAddOrEditBtn": [
		{
			"componentAttr": {
				"type": "text"
			},
			"dataType": "string",
			"dynmicMetaAttrSettings": {
				"canCommitWithEventNames": [
					"EditRowDialogFormOpen"
				],
				"isShowWithEventNames": []
			},
			"key": "id",
			"label": "id",
			"type": "Text"
		},
		{
			"componentAttr": {
				"type": "text"
			},
			"dataType": "string",
			"dynmicMetaAttrSettings": {
				"disabledWithEventNames": [],
				"isShowWithEventNames": [
					"buildInAddBtn",
					"buildInEditBtn"
				],
				"readonlyWithEventNames": []
			},
			"key": "moduleCode",
			"label": "模块code",
			"type": "Input"
		},
		{
			"componentAttr": {
				"type": "text"
			},
			"dataType": "string",
			"dynmicMetaAttrSettings": {
				"disabledWithEventNames": [],
				"isShowWithEventNames": [
					"buildInAddBtn",
					"buildInEditBtn"
				],
				"readonlyWithEventNames": []
			},
			"key": "version",
			"label": "版本号",
			"type": "Input"
		},
		{
			"componentAttr": {
				"type": "text"
			},
			"dataType": "string",
			"dynmicMetaAttrSettings": {
				"disabledWithEventNames": [],
				"isShowWithEventNames": [
					"buildInAddBtn",
					"buildInEditBtn"
				],
				"readonlyWithEventNames": []
			},
			"key": "displayName",
			"label": "模块名称",
			"type": "Input"
		},
		{
			"componentAttr": {
				"type": "textarea"
			},
			"dataType": "string",
			"dynmicMetaAttrSettings": {
				"disabledWithEventNames": [],
				"isShowWithEventNames": [
					"buildInAddBtn",
					"buildInEditBtn"
				],
				"readonlyWithEventNames": []
			},
			"key": "descrip",
			"label": "描述",
			"type": "Input"
		},
		{
			"componentAttr": {
				"type": "number"
			},
			"dataType": "hack_decimal-string",
			"dynmicMetaAttrSettings": {
				"disabledWithEventNames": [],
				"isShowWithEventNames": [
					"buildInAddBtn",
					"buildInEditBtn"
				],
				"readonlyWithEventNames": []
			},
			"key": "price",
			"label": "价格",
			"type": "Input"
		},
		{
			"componentAttr": {
				"type": "text"
			},
			"dataType": "bool",
			"dynmicMetaAttrSettings": {
				"disabledWithEventNames": [],
				"isShowWithEventNames": [
					"buildInAddBtn",
					"buildInEditBtn"
				],
				"readonlyWithEventNames": []
			},
			"key": "isSourceCode",
			"label": "是否提供源码",
			"type": "Switch"
		},
		{
			"componentAttr": {
				"type": "text"
			},
			"dataType": "bool",
			"dynmicMetaAttrSettings": {
				"disabledWithEventNames": [],
				"isShowWithEventNames": [
					"buildInAddBtn",
					"buildInEditBtn"
				],
				"readonlyWithEventNames": []
			},
			"key": "isFreeUpdate",
			"label": "是否免费更新",
			"type": "Switch"
		},
		{
			"componentAttr": {
				"type": "number"
			},
			"dataType": "hack_decimal-string",
			"dynmicMetaAttrSettings": {
				"disabledWithEventNames": [],
				"isShowWithEventNames": [
					"buildInAddBtn",
					"buildInEditBtn"
				],
				"readonlyWithEventNames": []
			},
			"key": "updatePride",
			"label": "更新价格",
			"type": "Input"
		}
	]
}