export default [
	{
		"componentAttr": {
			"type": "text"
		},
		"isCustomField": false,
		"isShow": false,
		"key": "id",
		"label": "id",
		"type": "Text"
	},
	{
		"componentAttr": {
			"type": "text"
		},
		"isCustomField": false,
		"isShow": true,
		"key": "moduleCode",
		"label": "模块code",
		"type": "Text"
	},
	{
		"componentAttr": {
			"type": "text"
		},
		"isCustomField": false,
		"isShow": true,
		"key": "version",
		"label": "版本号",
		"type": "Text"
	},
	{
		"componentAttr": {
			"type": "text"
		},
		"isCustomField": false,
		"isShow": true,
		"key": "displayName",
		"label": "模块名称",
		"type": "Text"
	},
	{
		"componentAttr": {
			"type": "text"
		},
		"isCustomField": false,
		"isShow": true,
		"key": "descrip",
		"label": "描述",
		"type": "Text"
	},
	{
		"componentAttr": {
			"type": "text"
		},
		"isCustomField": false,
		"isShow": true,
		"key": "price",
		"label": "价格",
		"type": "Text"
	},
	{
		"componentAttr": {
			"type": "text"
		},
		"isCustomField": false,
		"isShow": true,
		"key": "isSourceCode",
		"label": "是否提供源码",
		"type": "Text"
	},
	{
		"componentAttr": {
			"type": "text"
		},
		"isCustomField": false,
		"isShow": true,
		"key": "isFreeUpdate",
		"label": "是否免费更新",
		"type": "Text"
	},
	{
		"componentAttr": {
			"type": "text"
		},
		"isCustomField": false,
		"isShow": true,
		"key": "updatePride",
		"label": "更新价格",
		"type": "Text"
	}
]