export default [
	{
		"key": "id"
	},
	{
		"key": "parentId"
	},
	{
		"key": "moduleCode"
	},
	{
		"key": "dictTypeCode"
	},
	{
		"key": "gdSysDictTypeName"
	},
	{
		"key": "gdSysDictTypeDescrip"
	},
	{
		"key": "ordinal"
	},
	{
		"key": "name"
	},
	{
		"key": "value"
	},
	{
		"key": "disabled"
	},
	{
		"key": "descrip"
	}
]