export default {
	"buildInAddOrEditBtn": [
		{
			"componentAttr": {
				"type": "text"
			},
			"dataType": "string",
			"dynmicMetaAttrSettings": {
				"canCommitWithEventNames": [
					"EditRowDialogFormOpen"
				],
				"isShowWithEventNames": []
			},
			"key": "id",
			"label": "id",
			"type": "Text"
		},
		{
			"componentAttr": {
				"type": "text"
			},
			"dataType": "string",
			"dynmicMetaAttrSettings": {
				"disabledWithEventNames": [],
				"isShowWithEventNames": [
					"buildInAddBtn",
					"buildInEditBtn"
				],
				"readonlyWithEventNames": []
			},
			"formatterFunc": "",
			"key": "parentId",
			"label": "parent_id",
			"type": "Text"
		},
		{
			"componentAttr": {
				"autosize": false,
				"clearable": false,
				"disabled": false,
				"readonly": true,
				"showPassword": false,
				"showWordLimit": false,
				"size": "default",
				"style": "{}",
				"type": "text"
			},
			"dataType": "string",
			"dynmicMetaAttrSettings": {
				"disabledWithEventNames": [],
				"isShowWithEventNames": [
					"buildInAddBtn",
					"buildInEditBtn"
				],
				"readonlyWithEventNames": [
					"buildInAddBtn",
					"buildInEditBtn"
				]
			},
			"key": "moduleCode",
			"label": "模块code",
			"type": "Input"
		},
		{
			"componentAttr": {
				"autosize": false,
				"clearable": false,
				"disabled": false,
				"readonly": true,
				"showPassword": false,
				"showWordLimit": false,
				"size": "default",
				"style": "{}",
				"type": "text"
			},
			"dataType": "string",
			"dynmicMetaAttrSettings": {
				"disabledWithEventNames": [],
				"isShowWithEventNames": [
					"buildInAddBtn",
					"buildInEditBtn"
				],
				"readonlyWithEventNames": [
					"buildInEditBtn"
				]
			},
			"key": "dictTypeCode",
			"label": "字典类型code",
			"type": "Input"
		},
		{
			"componentAttr": {
				"type": "text"
			},
			"dataType": "string",
			"dynmicMetaAttrSettings": {
				"disabledWithEventNames": [],
				"isShowWithEventNames": [],
				"readonlyWithEventNames": []
			},
			"key": "gdSysDictTypeName",
			"label": "name",
			"searchKey": "dictTypeCode.name",
			"type": "Input"
		},
		{
			"componentAttr": {
				"type": "text"
			},
			"dataType": "string",
			"dynmicMetaAttrSettings": {
				"disabledWithEventNames": [],
				"isShowWithEventNames": [],
				"readonlyWithEventNames": []
			},
			"key": "gdSysDictTypeDescrip",
			"label": "描述",
			"searchKey": "dictTypeCode.descrip",
			"type": "Input"
		},
		{
			"componentAttr": {
				"type": "number"
			},
			"dataType": "int",
			"dynmicMetaAttrSettings": {
				"disabledWithEventNames": [],
				"isShowWithEventNames": [
					"buildInAddBtn",
					"buildInEditBtn"
				],
				"readonlyWithEventNames": []
			},
			"key": "ordinal",
			"label": "顺序号",
			"type": "Input"
		},
		{
			"componentAttr": {
				"autosize": false,
				"clearable": false,
				"disabled": false,
				"readonly": false,
				"showPassword": false,
				"showWordLimit": false,
				"size": "default",
				"style": "{}",
				"type": "text"
			},
			"dataType": "string",
			"dynmicMetaAttrSettings": {
				"disabledWithEventNames": [],
				"isShowWithEventNames": [
					"buildInAddBtn",
					"buildInEditBtn"
				],
				"readonlyWithEventNames": []
			},
			"key": "name",
			"label": "name",
			"type": "Input"
		},
		{
			"componentAttr": {
				"type": "text"
			},
			"dataType": "string",
			"dynmicMetaAttrSettings": {
				"disabledWithEventNames": [],
				"isShowWithEventNames": [
					"buildInAddBtn",
					"buildInEditBtn"
				],
				"readonlyWithEventNames": []
			},
			"key": "value",
			"label": "value",
			"type": "Input"
		},
		{
			"componentAttr": {
				"type": "text"
			},
			"dataType": "bool",
			"dynmicMetaAttrSettings": {
				"disabledWithEventNames": [],
				"isShowWithEventNames": [
					"buildInAddBtn",
					"buildInEditBtn"
				],
				"readonlyWithEventNames": []
			},
			"key": "disabled",
			"label": "disabled",
			"type": "Switch"
		},
		{
			"componentAttr": {
				"type": "textarea"
			},
			"dataType": "string",
			"dynmicMetaAttrSettings": {
				"disabledWithEventNames": [],
				"isShowWithEventNames": [
					"buildInAddBtn",
					"buildInEditBtn"
				],
				"readonlyWithEventNames": []
			},
			"key": "descrip",
			"label": "描述",
			"type": "Input"
		}
	]
}