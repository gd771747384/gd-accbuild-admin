export default [
	{
		"componentAttr": {
			"type": "text"
		},
		"formatterFunc": "",
		"isCustomField": false,
		"isShow": false,
		"key": "id",
		"label": "id",
		"type": "Text"
	},
	{
		"componentAttr": {
			"type": "text"
		},
		"formatterFunc": "",
		"isCustomField": false,
		"isShow": false,
		"key": "parentId",
		"label": "parent_id",
		"type": "Text"
	},
	{
		"componentAttr": {
			"type": "text"
		},
		"isCustomField": false,
		"isShow": false,
		"key": "moduleCode",
		"label": "模块code",
		"type": "Text"
	},
	{
		"componentAttr": {
			"type": "text"
		},
		"formatterFunc": "\n\nreturn configObj.rowData[configObj.metaKey]\n\n",
		"isCustomField": false,
		"isShow": true,
		"key": "dictTypeCode",
		"label": "字典类型code",
		"type": "Text"
	},
	{
		"componentAttr": {
			"type": "text"
		},
		"formatter": "\nreturn \"aaaaa\"\n",
		"isCustomField": false,
		"isShow": true,
		"key": "gdSysDictTypeName",
		"label": "name",
		"searchKey": "dictTypeCode.name",
		"type": "Text"
	},
	{
		"componentAttr": {
			"type": "text"
		},
		"formatterFunc": "",
		"isCustomField": false,
		"isShow": false,
		"key": "gdSysDictTypeDescrip",
		"label": "描述",
		"searchKey": "dictTypeCode.descrip",
		"type": "Text"
	},
	{
		"componentAttr": {
			"type": "number"
		},
		"isCustomField": false,
		"isShow": true,
		"key": "ordinal",
		"label": "顺序号",
		"type": "Text"
	},
	{
		"componentAttr": {
			"type": "text"
		},
		"isCustomField": false,
		"isShow": true,
		"key": "name",
		"label": "name",
		"type": "Text"
	},
	{
		"componentAttr": {
			"type": "text"
		},
		"isCustomField": false,
		"isShow": true,
		"key": "value",
		"label": "value",
		"type": "Text"
	},
	{
		"componentAttr": {
			"type": "text"
		},
		"isCustomField": false,
		"isShow": true,
		"key": "disabled",
		"label": "disabled",
		"type": "Text"
	},
	{
		"componentAttr": {
			"type": "text"
		},
		"isCustomField": false,
		"isShow": true,
		"key": "descrip",
		"label": "描述",
		"type": "Text"
	}
]