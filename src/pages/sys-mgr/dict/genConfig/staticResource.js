export default {
	"_id": "62db489b692769000144e744",
	"children": [],
	"code": "SysMgr.Dict",
	"descrip": null,
	"hasChildren": false,
	"icon": null,
	"id": "0000000000000000006",
	"isEnable": true,
	"isGranted": true,
	"isLeaf": true,
	"isShow": false,
	"moduleCode": "gd_sys",
	"name": "字典管理",
	"ordinal": 20,
	"pagePath": "sys-mgr/dict",
	"parentId": "0000000000000000001",
	"resourceType": "Page",
	"route": null,
	"targetPlatform": "pc-admin",
	"uiOptions": {
		"layoutStyle": "",
		"pageLayoutConfig": {
			"allColConfigs": [
				{
					"allColConfigs": [
						{
							"_children": "[]",
							"_slotCrudCompId": "1",
							"offset": 0,
							"span": 24,
							"style": {
								"height": "100%"
							}
						}
					],
					"rowConfig": {
						"align": "top",
						"gutter": 0,
						"justify": "start",
						"style": {
							"height": "50vh",
							"margin": "10px"
						}
					}
				},
				{
					"allColConfigs": [
						{
							"_children": "[]",
							"_slotCrudCompId": "2",
							"offset": 0,
							"span": 24,
							"style": {
								"height": "calc(45vh - 41px)"
							}
						}
					],
					"rowConfig": {
						"align": "top",
						"gutter": 0,
						"justify": "start",
						"style": {
							"height": "50vh",
							"margin": "10px"
						}
					}
				}
			],
			"rowConfig": {
				"align": "top",
				"gutter": 0,
				"justify": "start",
				"style": {
					"height": "100vh - #{$--accbuild-page-top-window-container-height}"
				}
			}
		},
		"pageStyle": "PageUiTemplate",
		"pageUiTemplateConfigs": [
			{
				"templateId": "1",
				"templateName": "BizSearchformTablelistSimple",
				"templateProps": {
					"filter_tableCommonAttr": {
						"appendFieldsIntoSearchForm": [
							{
								"orgKey": "parentId",
								"targetKey": "moduleCode"
							},
							{
								"orgKey": "code",
								"targetKey": "dictTypeCode"
							}
						],
						"dataFromType": "VmCode",
						"fillFieldsIntoAddForm": [
							{
								"orgKey": "parentId",
								"targetKey": "moduleCode"
							},
							{
								"orgKey": "code",
								"targetKey": "dictTypeCode"
							}
						],
						"labelField": "name",
						"lazy": true,
						"maxLevel": 2,
						"moduleCode": "gd_sys",
						"strategys": {},
						"treeSelectionType": "Multi",
						"treeTitle": "字典类型",
						"vmCode": "DictType"
					},
					"tableCommonAttr": {
						"dataFromType": "VmCode",
						"isEnterReqData": false,
						"moduleCode": "gd_sys",
						"paginationConfig": {
							"isPaged": false,
							"pageObj": {
								"pageNum": 0,
								"pageSize": 0,
								"totalCount": 0
							}
						},
						"size": "default",
						"vmCode": "DictData"
					}
				}
			},
			{
				"templateId": "2",
				"templateName": "BizSearchformTablelistSimple",
				"templateProps": {
					"tableCommonAttr": {
						"dataFromType": "VmCode",
						"isEnterReqData": true,
						"moduleCode": "gd_sys",
						"treeTableConfig": {
							"lazy": true
						},
						"vmCode": "Resource"
					}
				},
				"title": "资源管理"
			}
		],
		"target": "_tab"
	}
}