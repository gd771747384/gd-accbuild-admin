export default {
	"buildInTreeAddOrEditBtn": [
		{
			"componentAttr": {
				"type": "text"
			},
			"dataType": "string",
			"dynmicMetaAttrSettings": {
				"canCommitWithEventNames": [
					"EditRowDialogFormOpen"
				],
				"isShowWithEventNames": []
			},
			"key": "id",
			"label": "id",
			"type": "Text"
		},
		{
			"componentAttr": {
				"type": "text"
			},
			"dataType": "string",
			"dynmicMetaAttrSettings": {
				"disabledWithEventNames": [],
				"isShowWithEventNames": [
					"buildInAddBtn",
					"buildInEditBtn"
				],
				"readonlyWithEventNames": []
			},
			"key": "parentId",
			"label": "parent_id",
			"type": "Text"
		},
		{
			"componentAttr": {
				"type": "text"
			},
			"dataType": "string",
			"dynmicMetaAttrSettings": {
				"disabledWithEventNames": [],
				"isShowWithEventNames": [
					"buildInAddBtn",
					"buildInEditBtn"
				],
				"readonlyWithEventNames": []
			},
			"key": "code",
			"label": "code",
			"type": "Input"
		},
		{
			"componentAttr": {
				"type": "text"
			},
			"dataType": "string",
			"dynmicMetaAttrSettings": {
				"disabledWithEventNames": [],
				"isShowWithEventNames": [
					"buildInAddBtn",
					"buildInEditBtn"
				],
				"readonlyWithEventNames": []
			},
			"key": "name",
			"label": "name",
			"type": "Input"
		},
		{
			"componentAttr": {
				"type": "textarea"
			},
			"dataType": "string",
			"dynmicMetaAttrSettings": {
				"disabledWithEventNames": [],
				"isShowWithEventNames": [
					"buildInAddBtn",
					"buildInEditBtn"
				],
				"readonlyWithEventNames": []
			},
			"key": "descrip",
			"label": "描述",
			"type": "Input"
		}
	]
}