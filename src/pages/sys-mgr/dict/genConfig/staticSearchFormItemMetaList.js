export default [
	{
		"componentAttr": {
			"autosize": false,
			"clearable": false,
			"disabled": false,
			"readonly": false,
			"showPassword": false,
			"showWordLimit": false,
			"size": "default",
			"style": "{}",
			"type": "text"
		},
		"dataType": "string",
		"isShow": true,
		"key": "value",
		"label": "value",
		"labelWidth": "0",
		"type": "Input"
	}
]