export default {
	"buildInAddOrEditBtn": [
		{
			"componentAttr": {
				"type": "text"
			},
			"dataType": "string",
			"dynmicMetaAttrSettings": {
				"canCommitWithEventNames": [
					"EditRowDialogFormOpen"
				],
				"isShowWithEventNames": []
			},
			"key": "id",
			"label": "id",
			"type": "Text"
		},
		{
			"componentAttr": {
				"appendSlotConfig": {},
				"autosize": false,
				"clearable": true,
				"disabled": false,
				"placeholder": "",
				"prefixSlotConfig": {},
				"readonly": false,
				"showPassword": false,
				"showWordLimit": false,
				"size": "default",
				"style": "{}",
				"type": "text"
			},
			"dataType": "string",
			"default": "",
			"dynmicMetaAttrSettings": {
				"disabledWithEventNames": [],
				"isShowWithEventNames": [
					"buildInAddBtn",
					"buildInEditBtn"
				],
				"readonlyWithEventNames": []
			},
			"events": {
				"change": "console.log('b的change事件',configObj)"
			},
			"eventsFormData": [],
			"key": "b",
			"label": "颜色",
			"metaEventListeners": [],
			"type": "Input"
		},
		{
			"componentAttr": {
				"appendSlotConfig": {},
				"autosize": false,
				"clearable": false,
				"disabled": false,
				"placeholder": "",
				"prefixSlotConfig": {
					"contentType": "CustomizeComp",
					"value": {
						"componentAttr": {},
						"componentFilePath": "gd-demox/atomic-ui/test2.vue",
						"default": ""
					}
				},
				"readonly": false,
				"showPassword": false,
				"showWordLimit": false,
				"size": "default",
				"style": "{}",
				"type": "text"
			},
			"dataType": "string",
			"default": "",
			"dynmicMetaAttrSettings": {
				"disabledWithEventNames": [],
				"isShowWithEventNames": [
					"buildInAddBtn",
					"buildInEditBtn"
				],
				"readonlyWithEventNames": []
			},
			"eventsFormData": [
				{
					"eventName": "_modelValueChange",
					"func": "console.log(configObj,'监听到b组件的绑定值变化')",
					"immediate": true,
					"metaKey": "b",
					"priority": 1
				},
				{
					"eventName": "clear",
					"func": "console.log('clear监听',configObj)",
					"immediate": false,
					"metaKey": "b",
					"order": 1
				}
			],
			"key": "c",
			"label": "c",
			"metaEventListeners": [
				{
					"eventName": "_modelValueChange",
					"func": "console.log(configObj,'监听到b组件的绑定值变化')",
					"immediate": true,
					"metaKey": "b",
					"priority": 1
				},
				{
					"eventName": "clear",
					"func": "console.log('clear监听',configObj)",
					"immediate": false,
					"metaKey": "b",
					"order": 1
				}
			],
			"type": "Input"
		}
	]
}