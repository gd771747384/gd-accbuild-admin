export default {
	"_id": "64886cd009e298a239f9c045",
	"code": "GdExample/table",
	"descrip": "单表页面",
	"icon": "collection",
	"id": "000000000000000015",
	"isEnable": true,
	"isShow": true,
	"moduleCode": "gd_sys",
	"name": "采购申请单",
	"ordinal": 1,
	"pagePath": "gd-example/table",
	"parentId": "uOAuMttdOlYASBLuA677Y",
	"resourceType": "Page",
	"uiOptions": {
		"layoutStyle": "",
		"pageStyle": "PageUiTemplate",
		"pageUiTemplateConfigs": [
			{
				"templateName": "BizSearchformTablelistSimple",
				"templateProps": {
					"tableCommonAttr": {
						"_gdAccbuild_GdSysProject_proxyFlag": true,
						"dataFromType": "VmCode",
						"isEnterReqData": true,
						"moduleCode": "gd_demox",
						"paginationConfig": {
							"isPaged": false,
							"pageObj": {
								"pageNum": 0,
								"pageSize": 0,
								"totalCount": 0
							}
						},
						"size": "default",
						"vmCode": "GdDemoxWw"
					}
				},
				"title": "资源管理"
			}
		],
		"target": "_tab"
	}
}