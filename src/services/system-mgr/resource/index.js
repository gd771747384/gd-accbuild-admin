import request from "@/utils/http";

import global from "@/config/global";
/**
 * 每个页面都需要的 增删改查接口
 *   */
import {
  isSuccessReqBackend,
  transName,
  getBackendDataItems,
  getBackendData,
  treeTools
} from "@/config/utils";
const moduleName = "SysMgr-ResourceMgr";
const pathFromModuleName = moduleName
  .split("-")
  .map((el) => transName(el, "Pascal", "Kebab"))
  .join("/");
const uniCloudFuncName = `${global.UniCloudPrefixName}-SysMgr-ResourceMgr`;
/** 用户资源接口 */
export async function getResourceList(params, lazy = false) {
  const res = await request({
    url: `${global.BASE_API_PREFIX}/${pathFromModuleName}/get-list`,
    method: "get",
    params,
    uniCloudFuncName,
    uniCloudFuncStrategyName: "getList",
  });
  //console.time('invertTree');
  const treeData = treeTools.invertTree({ sourceArr: getBackendDataItems(res), lazy });
  const ret = {
    code: res.code,
    data: {
      items: treeData.result,
      allNotCompleteLazyResult: treeData.allNotCompleteLazyResult,
      flattenData: getBackendDataItems(res),
    },
  };
  //console.timeEnd('invertTree');
  return ret;
}
