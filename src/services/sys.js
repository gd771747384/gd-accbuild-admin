import request from "@/utils/http";
import global from "@/config/global";
import { getResourceList } from "./system-mgr/resource/index";
import {
  kebabHasChildrenAlias,
  childrenKeyAlias,
  idKeyAlias,
  pidKeyAlias,
  kebabPidAlias,
  uiOptionsKeyAlias,
} from "@/config/resourceApi";
import { treeTools } from "@/config/utils";
import {
  isSuccessReqBackend,
  transName,
  getBackendDataItems,
  getBackendData,
} from "@/config/utils";
import { generateBizCrud } from "./biz-crud/index";
const moduleName = "sys";
const pathFromModuleName = moduleName
  .split("-")
  .map((el) => transName(el, "Pascal", "Kebab"))
  .join("/");
const cloudFuncName = `${global.UniCloudPrefixName}-${moduleName}`;

export function login(data) {
  return request({
    url: `${global.BASE_API_PREFIX}/${pathFromModuleName}/login`,
    method: "post",
    data,
    cloudFuncName,
    cloudFuncStrategyName: "login",
  });
}

export function loginByOath2(data) {
  return request({
    url: `${global.BASE_API_PREFIX}/${pathFromModuleName}/loginByOath2`,
    method: "post",
    data,
    cloudFuncName,
    cloudFuncStrategyName: "loginByOath2",
  });
}

export function register(data) {
  return request({
    url: `${global.BASE_API_PREFIX}/${pathFromModuleName}/register`,
    method: "post",
    data,
    cloudFuncName,
    cloudFuncStrategyName: "register",
  });
}
export function sendVerifycode(data) {
  return request({
    url: `${global.BASE_API_PREFIX}/${pathFromModuleName}/send-verifycode`,
    method: "post",
    data,
    cloudFuncName,
    cloudFuncStrategyName: "send-verifycode",
  });
}

/**
 * 按策略名请求视图模型;支持代理到客户端的请求
 */
export function requestByVmStrategyName({
  moduleCode = "gd_sys",
  vmCode,
  strategyName,
  requestType = "post",
  data,
  vmTemplateConfig,
}) {
  return generateBizCrud({
    methodName: "requestByVmStrategyName",
    moduleCode,
    vmCode,
    strategyName,
    requestType,
    params: {},
    data,
    vmTemplateConfig,
  });
}

export function logout() {
  return request({
    url: "/sys/logout",
    method: "post",
  });
}

// 首页相关api

export function getUserInfo() {
  return request({
    url: "/sys/user/info",
    method: "get",
  });
}
// /api/sys/user/password

export function updateUserPassword(data) {
  return request({
    url: "/sys/user/password",
    method: "post",
    data,
  });
}

/**
 * 获取对应vmCode的元信息
 * 每个页面表格的表头 以及 搜索表单、新增编辑表单的配置信息
 */
/* export function getMetaListByVmCode(params) {
    return request({
        url:`${global.BASE_API_PREFIX}/${pathFromModuleName}/get-meta-list`,
        method: 'get',
        params: { ...params, sortBy: "ordinal asc" },//\"Ordinal\"
        cloudFuncName,
        cloudFuncStrategyName: "getMetaList"
    })
} */
export function getMetaListByVmCode(params, vmTemplateConfig) {
  /* if(!params.vmCode){
    return null
  } */
  return generateBizCrud({
    methodName: "getMetaListByVmCode",
    moduleCode: params.moduleCode,
    vmCode: params.vmCode,
    params,
    data: {},
    vmTemplateConfig,
  });
}

/** 当前用户是否选择显示隐藏 */
export function hiddenUserColumns(data) {
  return request({
    url: `${global.BASE_API_PREFIX}/user/hide-columns`,
    method: "post",
    data,
  });
}

/** 用户资源接口 */
export async function getUserResource(params, lazy = false) {
  const url = `${global.BASE_API_PREFIX}/${pathFromModuleName}/get-resource`;
  const res = await request({
    url,
    method: "get",
    params,
    cloudFuncName,
    cloudFuncStrategyName: "getResourceList",
  });
  //console.time('invertTree');
  const treeData = treeTools.invertTree({ sourceArr: getBackendDataItems(res), lazy });
  const ret = {
    code: res.code,
    data: {
      items: treeData.result,
      allNotCompleteLazyResult: treeData.allNotCompleteLazyResult,
    },
  };
  //console.timeEnd('invertTree');
  return ret;
}

export function get({ moduleCode, vmCode, params = {}, vmTemplateConfig }) {
  /* if(!params.vmCode){
    return null
  } */
  return generateBizCrud({
    methodName: "get",
    moduleCode,
    vmCode,
    params,
    data: {},
    vmTemplateConfig,
  });
}

export function getList({ moduleCode, vmCode, params = {}, vmTemplateConfig }) {
  /* if(!params.vmCode){
    return null
  } */
  return generateBizCrud({
    methodName: "getList",
    moduleCode,
    vmCode,
    params,
    data: {},
    vmTemplateConfig,
  });
}

export function save({ moduleCode, vmCode, data, vmTemplateConfig }) {
  /* if(!params.vmCode){
    return null
  } */
  return generateBizCrud({
    methodName: "save",
    moduleCode,
    vmCode,
    params: {},
    data,
    vmTemplateConfig,
  });
}

export function update({ moduleCode, vmCode, data, vmTemplateConfig }) {
  /* if(!params.vmCode){
    return null
  } */
  return generateBizCrud({
    methodName: "update",
    moduleCode,
    vmCode,
    params: {},
    data,
    vmTemplateConfig,
  });
}
export function deleteBatch({ moduleCode, vmCode, params, vmTemplateConfig }) {
  /* if(!params.vmCode){
    return null
  } */
  return generateBizCrud({
    methodName: "deleteBatch",
    moduleCode,
    vmCode,
    params,
    data: {},
    vmTemplateConfig,
  });
}

/**
 * 获取编号的接口
 * @param {*} data
 * @returns
 */
export function generateCode({
  moduleCode = "gd_sys",
  vmCode = "Common",
  strategyName = "generateCode",
  requestType = "post",
  data = {
    tableName: "gd_sys_project",
    fieldName: "project_code",
    generateAlgorithm: "regex", //"nanoid"、"uuid"、"regex"
    expression: "^[a-z]([a-z]|_){1,23}[a-z]$",
    isUnique: true,
    value: "",
  },
}) {
  return generateBizCrud({
    methodName: "requestByVmStrategyName",
    moduleCode,
    vmCode,
    strategyName,
    requestType,
    params: {},
    data,
  });
}
///////////////////
export const globalHttpApis = {
  getResourceList, //所有资源;autogen会用到
  requestByVmStrategyName,
  getMetaListByVmCode,
  get,
  getList,
  save,
  update,
  deleteBatch,
  generateCode,
  getUserResource,
};
