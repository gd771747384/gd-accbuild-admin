import request from '@/utils/http'
import global from '@/config/global';

export function importEntityModel(data) {
    return request({
        url: `${global.BASE_API_PREFIX}/acmen/sync-table-to-entity`,
        method: 'post',
        data
    })
}