import request from '@/utils/http'
import global from '@/config/global';

export function generateCodeFiles(data) {
    return request({
        url: `${global.BASE_API_PREFIX}/acmen/build-model`,
        method: 'post',
        data
    })
}