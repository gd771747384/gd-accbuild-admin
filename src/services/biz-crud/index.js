
import {
    isSuccessReqBackend,
    transName,
    getBackendDataItems,
    getBackendData,
} from '@/config/utils'
import request from '@/utils/http'
import global from '@/config/global';

/**
 * 根据key 获取地址栏query的value
 */
export function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    let searchStr = ""
    if (window.location.search) {
        searchStr = window.location.search.substring(1)
    } else {
        const flagIdx = window.location.hash.indexOf("?")
        if (flagIdx !== -1) {
            searchStr = window.location.hash.substring(flagIdx + 1)
        }
    }
    var r = searchStr.match(reg);
    if (r != null) return decodeURIComponent(decodeURIComponent(r[2])); return null;
}

const moduleName = "sys"
const pathFromModuleName = moduleName.split('-').map(el => transName(el, "Pascal", "Kebab")).join('/')
const cloudFuncName = `${global.UniCloudPrefixName}-${moduleName}`

export const generateBizCrud = async ({ moduleCode, vmCode, strategyName, requestType, methodName, params, data, vmTemplateConfig }) => {
    const kebabVmCode = vmCode ? transName(vmCode, "Pascal", "Kebab") : null
    const strategyExecutor = {
        requestByVmStrategyName: () => {
            const appendObj = {}
            requestType === "post" ? appendObj.data = data : appendObj.params = data
            const mustAppendParams = {
                ...appendObj,
                cloudFuncName,
                strategyName,
                moduleCode,
                vmCode
            }
            if (typeof vmTemplateConfig === "object" && vmTemplateConfig._gdAccbuild_GdSysProject_proxyFlag) {
                const spaceId = getQueryString("spaceId")
                const gdAccbuildUnicloudSpacesStr = uni.getStorageSync("gdAccbuildUnicloudSpaces");
                const gdAccbuildUnicloudSpaces = gdAccbuildUnicloudSpacesStr ? JSON.parse(gdAccbuildUnicloudSpacesStr) : [];
                const matchedSpace = gdAccbuildUnicloudSpaces.find(el => el.id === spaceId)
                return request({
                    url: `${global.BASE_API_PREFIX}/${pathFromModuleName}/${requestType}`,
                    //method: requestType,
                    ...appendObj,
                    data: {
                        ...mustAppendParams,
                        projectCode: getQueryString("projectCode"),
                        proxyUrl: getQueryString("proxyUrl"),
                        projectSpaceId: spaceId,
                        projectProvider: matchedSpace?.provider ?? "aliyun",
                    },
                    method: requestType,
                    cloudFuncName: "gd-sys",
                    cloudFuncStrategyName: "sendBizCrudHttpProxy",
                    moduleCode: "gd_sys",
                    vmCode: "GdSysProject"
                })
            }
            return request({
                url: `${global.BASE_API_PREFIX}/${pathFromModuleName}/${requestType}`,
                method: requestType,
                ...mustAppendParams,
                cloudFuncStrategyName: mustAppendParams["strategyName"],
            })
        },
        getMetaListByVmCode: () => {
            const mustAppendParams = {
                params: { ...params, sortBy: "ordinal asc" },//\"Ordinal\"
                cloudFuncName,
                strategyName: "getMetaList",
                moduleCode,
            }
            if (typeof vmTemplateConfig === "object" && vmTemplateConfig._gdAccbuild_GdSysProject_proxyFlag) {
                const spaceId = getQueryString("spaceId")
                const gdAccbuildUnicloudSpacesStr = uni.getStorageSync("gdAccbuildUnicloudSpaces");
                const gdAccbuildUnicloudSpaces = gdAccbuildUnicloudSpacesStr ? JSON.parse(gdAccbuildUnicloudSpacesStr) : [];
                const matchedSpace = gdAccbuildUnicloudSpaces.find(el => el.id === spaceId)

                return request({
                    url: `${global.BASE_API_PREFIX}/${kebabVmCode}/get`,
                    params,
                    data: {
                        ...mustAppendParams,
                        projectCode: getQueryString("projectCode"),
                        proxyUrl: getQueryString("proxyUrl"),
                        projectSpaceId: spaceId,
                        projectProvider: matchedSpace?.provider ?? "aliyun",

                    },
                    method: 'get',
                    cloudFuncName: "gd-sys",
                    cloudFuncStrategyName: "sendBizCrudHttpProxy",
                    moduleCode: "gd_sys",
                    vmCode: "GdSysProject"
                })
            }
            return request({
                url: `${global.BASE_API_PREFIX}/sys/get-meta-list`,
                method: 'get',
                ...mustAppendParams,
                cloudFuncStrategyName: mustAppendParams["strategyName"],
            })
        },
        get: () => {
            const mustAppendParams = {
                params,
                cloudFuncName,
                strategyName: "getByVmCode",
                moduleCode,
                vmCode,
            }
            if (typeof vmTemplateConfig === "object" && vmTemplateConfig._gdAccbuild_GdSysProject_proxyFlag) {
                const spaceId = getQueryString("spaceId")
                const gdAccbuildUnicloudSpacesStr = uni.getStorageSync("gdAccbuildUnicloudSpaces");
                const gdAccbuildUnicloudSpaces = gdAccbuildUnicloudSpacesStr ? JSON.parse(gdAccbuildUnicloudSpacesStr) : [];
                const matchedSpace = gdAccbuildUnicloudSpaces.find(el => el.id === spaceId)
                return request({
                    url: `${global.BASE_API_PREFIX}/${kebabVmCode}/get`,
                    params,
                    data: {
                        ...mustAppendParams,
                        projectCode: getQueryString("projectCode"),
                        proxyUrl: getQueryString("proxyUrl"),
                        projectSpaceId: spaceId,
                        projectProvider: matchedSpace?.provider ?? "aliyun",
                    },
                    method: 'get',
                    cloudFuncName: "gd-sys",
                    cloudFuncStrategyName: "sendBizCrudHttpProxy",
                    moduleCode: "gd_sys",
                    vmCode: "GdSysProject"
                })
            }
            return request({
                url: `${global.BASE_API_PREFIX}/${kebabVmCode}/get`,
                method: 'get',
                ...mustAppendParams,
                cloudFuncStrategyName: mustAppendParams["strategyName"],
            })
        },
        getList: () => {
            const mustAppendParams = {
                params,
                cloudFuncName,
                strategyName: "getListByVmCode",
                moduleCode,
                vmCode,
            }
            if (typeof vmTemplateConfig === "object" && vmTemplateConfig._gdAccbuild_GdSysProject_proxyFlag) {
                const spaceId = getQueryString("spaceId")
                const gdAccbuildUnicloudSpacesStr = uni.getStorageSync("gdAccbuildUnicloudSpaces");
                const gdAccbuildUnicloudSpaces = gdAccbuildUnicloudSpacesStr ? JSON.parse(gdAccbuildUnicloudSpacesStr) : [];
                const matchedSpace = gdAccbuildUnicloudSpaces.find(el => el.id === spaceId)
                return request({
                    url: `${global.BASE_API_PREFIX}/${kebabVmCode}/get-list`,
                    params,
                    data: {
                        ...mustAppendParams,
                        projectCode: getQueryString("projectCode"),
                        proxyUrl: getQueryString("proxyUrl"),
                        projectSpaceId: spaceId,
                        projectProvider: matchedSpace?.provider ?? "aliyun",
                    },
                    method: 'get',
                    cloudFuncName: "gd-sys",
                    cloudFuncStrategyName: "sendBizCrudHttpProxy",
                    moduleCode: "gd_sys",
                    vmCode: "GdSysProject"
                })
            }
            return request({
                url: `${global.BASE_API_PREFIX}/${kebabVmCode}/get-list`,
                method: 'get',
                ...mustAppendParams,
                cloudFuncStrategyName: mustAppendParams["strategyName"],
            })
        },
        save: () => {
            const mustAppendParams = {
                data,
                cloudFuncName,
                strategyName: "insertByVmCode",
                moduleCode,
                vmCode,
            }
            if (typeof vmTemplateConfig === "object" && vmTemplateConfig._gdAccbuild_GdSysProject_proxyFlag) {
                const spaceId = getQueryString("spaceId")
                const gdAccbuildUnicloudSpacesStr = uni.getStorageSync("gdAccbuildUnicloudSpaces");
                const gdAccbuildUnicloudSpaces = gdAccbuildUnicloudSpacesStr ? JSON.parse(gdAccbuildUnicloudSpacesStr) : [];
                const matchedSpace = gdAccbuildUnicloudSpaces.find(el => el.id === spaceId)
                return request({
                    url: `${global.BASE_API_PREFIX}/${kebabVmCode}/create`,
                    params,
                    data: {
                        ...mustAppendParams,
                        projectCode: getQueryString("projectCode"),
                        proxyUrl: getQueryString("proxyUrl"),
                        projectSpaceId: spaceId,
                        projectProvider: matchedSpace?.provider ?? "aliyun",
                    },
                    method: 'post',
                    cloudFuncName: "gd-sys",
                    cloudFuncStrategyName: "sendBizCrudHttpProxy",
                    moduleCode: "gd_sys",
                    vmCode: "GdSysProject"
                })
            }
            return request({
                url: `${global.BASE_API_PREFIX}/${kebabVmCode}/create`,
                method: 'post',
                ...mustAppendParams,
                cloudFuncStrategyName: mustAppendParams["strategyName"],
            })
        },
        update: () => {
            const mustAppendParams = {
                data,
                cloudFuncName,
                strategyName: "updateByVmCode",
                moduleCode,
                vmCode,
            }
            if (typeof vmTemplateConfig === "object" && vmTemplateConfig._gdAccbuild_GdSysProject_proxyFlag) {
                const spaceId = getQueryString("spaceId")
                const gdAccbuildUnicloudSpacesStr = uni.getStorageSync("gdAccbuildUnicloudSpaces");
                const gdAccbuildUnicloudSpaces = gdAccbuildUnicloudSpacesStr ? JSON.parse(gdAccbuildUnicloudSpacesStr) : [];
                const matchedSpace = gdAccbuildUnicloudSpaces.find(el => el.id === spaceId)
                return request({
                    url: `${global.BASE_API_PREFIX}/${kebabVmCode}/update`,
                    params,
                    data: {
                        ...mustAppendParams,
                        projectCode: getQueryString("projectCode"),
                        proxyUrl: getQueryString("proxyUrl"),
                        projectSpaceId: spaceId,
                        projectProvider: matchedSpace?.provider ?? "aliyun",
                    },
                    method: 'put',
                    cloudFuncName: "gd-sys",
                    cloudFuncStrategyName: "sendBizCrudHttpProxy",
                    moduleCode: "gd_sys",
                    vmCode: "GdSysProject"
                })
            }
            return request({
                url: `${global.BASE_API_PREFIX}/${kebabVmCode}/update`,
                method: 'put',
                ...mustAppendParams,
                cloudFuncStrategyName: mustAppendParams["strategyName"],
            })
        },
        deleteBatch: () => {
            const mustAppendParams = {
                params,
                cloudFuncName,
                strategyName: "deleteBatchByVmCode",
                moduleCode,
                vmCode,
            }
            if (typeof vmTemplateConfig === "object" && vmTemplateConfig._gdAccbuild_GdSysProject_proxyFlag) {
                const spaceId = getQueryString("spaceId")
                const gdAccbuildUnicloudSpacesStr = uni.getStorageSync("gdAccbuildUnicloudSpaces");
                const gdAccbuildUnicloudSpaces = gdAccbuildUnicloudSpacesStr ? JSON.parse(gdAccbuildUnicloudSpacesStr) : [];
                const matchedSpace = gdAccbuildUnicloudSpaces.find(el => el.id === spaceId)
                return request({
                    url: `${global.BASE_API_PREFIX}/${kebabVmCode}/batch-delete`,
                    params,
                    data: {
                        ...mustAppendParams,
                        projectCode: getQueryString("projectCode"),
                        proxyUrl: getQueryString("proxyUrl"),
                        projectSpaceId: spaceId,
                        projectProvider: matchedSpace?.provider ?? "aliyun",
                    },
                    method: 'put',
                    cloudFuncName: "gd-sys",
                    cloudFuncStrategyName: "sendBizCrudHttpProxy",
                    moduleCode: "gd_sys",
                    vmCode: "GdSysProject"
                })
            }
            return request({
                url: `${global.BASE_API_PREFIX}/${kebabVmCode}/batch-delete`,
                method: 'delete',
                ...mustAppendParams,
                cloudFuncStrategyName: mustAppendParams["strategyName"],
            })
        }
    }
    return strategyExecutor[methodName]()
}
