/**
 * 由脚本自动生成;主要用于 H5 的跳转页面时,是否启用布局
 */
export default [{
    path: "pages/home/index",
    alias: ["", "/"],
    layoutStyle: ""
}, {
    path: "pages/login/login",
    alias: [],
    layoutStyle: "none"
}, {
    path: "pages/gd-accbuild-development-mgr/page-designer/index",
    alias: [],
    layoutStyle: "none"
},{
    path: "pages/gd-accbuild-development-mgr/home/index",
    alias: ["", "/"],
    layoutStyle: ""
},{
    path: "uni_modules/gd-accbuild-core/pages/cross-platform/default-crud-pages/index",
    alias: [],
    layoutStyle: ""
},{
    path: "pages/sys-mgr/resource/index",
    alias: [],
    layoutStyle: ""
},{
    path: "pages/sys-mgr/dict/index",
    alias: [],
    layoutStyle: ""
},{
    path: "pages/sys-mgr/own-module/index",
    alias: [],
    layoutStyle: ""
}, {
    path: "pages/gd-accbuild-development-mgr/view-model/index",
    alias: [],
    layoutStyle: ""
}, {
    path: "pages/gd-accbuild-development-mgr/entity-model/index",
    alias: [],
    layoutStyle: ""
}]