import manifestJSON from "../manifest.json";

import pagesLayout from "./pages-layout";
class GD_Log {
  constructor({ flag = "default", level = "log" }) {
    this.flag = flag;
    this.level = level;
  }
  println(...infos) {
    console.warn();
    console[this.level](`[${this.flag}]：`, ...infos, `.\n`);
  }
}
const baseRouterPrefixPath = manifestJSON.h5.router.base.substring(
  0,
  manifestJSON.h5.router.base.length - 1
);
const global = {
  UniCloudPrefixName: "gd",
  //后端请求基地址
  BASE_URL:
    process.env.NODE_ENV === "production"
      ? `http://localhost:9511`
      : `http://localhost:9511`, // http://localhost:9511
  BASE_API_PREFIX: process.env.NODE_ENV === "production" ? "/api" : "/api",
  BASE_ROUTER_PREFIX_PATH: baseRouterPrefixPath,
  BASE_STATIC_PREFIX_PATH:
    process.env.NODE_ENV === "production" ? baseRouterPrefixPath : "",
  ROUTER_MODE: manifestJSON.h5.router.mode,
  enableServerless: true,
  GD_Log,
  targetPlatform: "pc-admin",
  OAUTH_INFO: {
    client_id: process.env.OAUTH_CLIENT_ID, //gs "4Zsaqs6xlN"   //zt-home "KJ0QE1xUNF"   //yy-home "HWLrX55j5f"
  },
  pagesLayout,
  isSuccessReqBackend: function (res) {
    return res.code === 0;
  },
  getErrorMsgReqBackend: function (res) {
    return res.msg ? res.msg : "请求失败";
  },
  getBackendDataItems: function (res) {
    if (this.isSuccessReqBackend(res)) {
      return res.data.items; // res.success // res.code===0
    } else {
      return [];
    }
  },
  getBackendData: function (res) {
    if (this.isSuccessReqBackend(res)) {
      return res.data.data;
    } else {
      return {};
    }
  },
  fakerReqBackend: async function () {
    return Promise.resolve({
      code: 0,
      data: {
        data: {},
        items: [],
      },
    });
  },
};

export default global;
