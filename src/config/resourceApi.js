import {
    resourceApiAliasConfig
} from '@uni_modules/gd-accbuild-core/config/index'
export { resourcePermsKeyDefinedRule } from "@uni_modules/gd-accbuild-core/config/index"
export const {
    resourceUrlKeyAlias,
    childrenKeyAlias,
    realPageFileKeyAlias,
    resourceTypeKeyAlias,
    resourcePermsKeyAlias,
    resourceNameAlias,
    resourceIconAlias,
    idKeyAlias,
    pidKeyAlias,
    kebabPidAlias,
    targetKeyAlias,
    hasChildrenAlias,
    kebabHasChildrenAlias,
    ordinalKeyAlias,
    uiOptionsKeyAlias,
} = resourceApiAliasConfig