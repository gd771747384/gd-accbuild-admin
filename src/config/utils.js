export {
    isSuccessReqBackend,
    getErrorMsgReqBackend,
    getBackendDataItems,
    getBackendData,
    fakerReqBackend,
    cloneDeep,
    eq,
    u_getTypeof,
    isJsonString,
    mapKeysToTarget,
    transName,
    //dynmicImport,
    StateHandler,
    treeTools,
  } from "@uni_modules/gd-accbuild-core/config/utils";
  import global from "./global";
  // 获取assets静态资源
  export const getAsset = ({ url, assetType = "images" }) => {
    return `${global.BASE_STATIC_PREFIX_PATH}/static/${assetType}/${url}`;
    //return new URL(`../static/${assetType}/${url}`, import.meta.url).href
  };
  