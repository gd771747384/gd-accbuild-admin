declare module '*.vue' {
    import { ComponentOptions,defineComponent } from 'vue'
    const component: ComponentOptions | ReturnType<typeof defineComponent>
    export default component
  }