# 资源表设计

         菜单  ------>     页面  ---->    按钮   、    接口
          |                                |

菜单级页面(含有 \_target)、普通菜单 弹窗按钮、按钮级页面(含有 \_target)、按钮级跳转(既有 \_target 又有 targetResourcePermsCode)

## 注意点

菜单级页面才会显示在底部的 tabbar 中,即带\_target 的菜单资源

什么是按钮级页面?
点击某个按钮进入的页面里面 没有权限资源,仅是表单类提交、详情页等;规则:且只有\_target 没有 targetResourcePermsCode

什么是按钮级跳转?
进入的页面里面还有更多的权限入口,不仅仅是普通表单,就需要跳转到另一个单独路由里面;规则:既有 \_target 又有 targetResourcePermsCode

realPageKey 截取规范,即所有"使用模板的"(没写 pagePath)页面的真实文件路径必须符合以下三种之一:
/pages/···菜单目录···/index.vue 菜单型页面文件 ;
/pages/···菜单目录···/···页面级目录(普通页面、按钮级页面)···/index.vue 页面文件 ;
/pages/···菜单目录···/···页面级目录(普通页面)···/···按钮级页面目录···/index.vue 按钮级页面文件 ;-----这种暂时不会出现

app、小程序的路由 url 不允许自定义 都必须与真实文件路径相同 ; h5 端(pc)的运行自定义 url
app、小程序的路由 url 除根据 code 后两位组成外还需要 最后的页面资源 path 还要加上 `/index` ; 且菜单页面 拼接路由表时 必须冗余 在冗余菜单页面里 加`/index`

# 设计计划

每个页面由 page-ui 中的页面模块组合;一个 page-ui 的 biz-ui 包含请求对应的 vmCode;
//如轮播型九宫格: 一屏列数、最大列数(大于一屏列数) 数据来源 dataList ({logo:"",title:""}),即 vmCode,查询条件(范围查询、包含某个值、正则(开始结尾))
## 组件分类 及 设计
布局组件、CRUD组件、原子组件

### 问题
如何将根据布局信息调整CRUD组件的次序？
利用 flex 的 order

## uiOptions 的配置

1.  getColumns 的每个字段的 uiOptions 需要配置：tableColumnUiConfig、searchFormItemUiConfig、dialogFormItemUiConfig
2.  页面级资源(菜单页面、普通页面、按钮页面)需要配置 uiOptions：主要需要配 pageUiTemplateConfigs
3.  按钮资源需要配 uiOptions：dynmicComponentConfigs-----注意 1、3 的配置格式一模一样
    注: vue2 旧版代码运行页面里些配置,现已经废弃

## propOptions 的配置

### tableConfig 的属性

`isOutput`: 说明字段是否存在,DTO 里面是否包含
`isDefaultShow`: 说明默认是否显示
`authRoles`
(`isShow`: 由后端经过权限一起作用后返回,根据 isOutput、isDefaultShow、authRoles 合并计算)

### searchformConfig 的属性

`isOutput`: 说明字段是否存在,DTO 里面是否包含
`authRoles`
(`isShow`: 由后端经过权限一起作用后返回,根据 isOutput、authRoles 合并计算)
`sortType`: 0 不允许排序,1 默认升序,2 默认降序
`searchType`: 0 不允许搜索,1 精准匹配,2 普通模糊匹配,3 前缀模糊匹配,4 后缀模糊匹配,5 范围查询

### btnsformConfig 的属性

`buildInAddBtn`:{isOutput,authRoles}//新增表单不需要每个字段设置权限,应该在新增按钮上设置权限即可
`buildInEditBtn`:{isOutput,authRoles}//更新表单根据每个用户角色不同,表单显示不同
`···`:{isOutput,authRoles}//这里 key 不能是 buildIn 开头
(`isShow`: 由后端经过权限一起作用后返回,根据 isOutput、authRoles 合并计算)

### relationConfig 的属性
1. 若某字段是外键字段，则存在`relationConfig`
2. `relationTypeCode`表示:
`relationConfig`同层级的`entityCode`+`entityPropCode` 关联到 `relationEntityCode`+`relationEntityPropCode` 的对应关系
其中 `relationEntityCode`+`relationEntityPropCode` 在同层级的字段里
3. `mappingProperties`关联表勾选的其它字段: 映射到最外层同层级的字段
其中 `overridePropOptions`不存在用原字段的 若存在则表示覆盖 同层级的对应字段 的viewProp的`propOptions`、`overrideUiOptions`不存在用原字段的 若存在则表示覆盖 同层级的对应字段 的viewProp的`uiOptions`
[{
    `relationEntityCode`://关联到的外键表名;对方的
    `relationEntityPropCode`://关联到的外键字段名;对方的
    `relationTypeCode`://关联过去的关联关系
    `midEntityCode`://中间表表名只有ManyToMany时,才会存在//这个中间表在uniCloud中不会利用;关联都通过foreignKey;是给导出到代码二开时使用的
    `uniCloudForeignKeyInEntityCode`://uniCloud专属的配置,说明外键 foreignKey 标记在 哪张表名的 dbSchema//且如果用到中间表则标记规定在中间表里
    `mappingProperties`:[{//映射到最外层同层级的字段
        //`entityCode`://与`relationEntityCode`相同
        `entityPropCode`:
        `overridePropOptions`:{
            ······
           `relationConfig`:[{//三级关联字段、可以按照相同原理无限递归下去
                ······
           }]
        }
        `overrideUiOptions`:
    }]
}]

* 注1: `n对多` 情况 只能用子表形式呈现(子表组件、多选组件);如果是 `n对一` 情况 则子表字段会被提升到主表同层级
* 注2: `relationConfig` 使用数组形式的原因: 同一个字段可能会被多次关联中使用
* 注3: 多对多解决方案: uniCloud后端不用中间表进行查询,统一使用一个冗余字段`gd_redundant_···`开头的,冗余字段放哪边,数组会不会太多
* 后端所有字段都必须不为null,起码不能插入null,前端传null即为不传;前端组件input清空 表示 不传 还是 空字符串,视情况而定,默认相当于不传, 空字符串 设计input后缀小勾选框
* 关联子表特别是 被引用表为主表时,需要更新子表对应字段
* 注4: `relationConfig` 这里 在ViewProp中 只存储表的关联关系，字典关联可以不存,因为后端用不到;而且特别要注意ViewProp里面的关联关系是这个关联子字段被勾选时,才会加入关联关系到`relationConfig`里面。
## gd_sys_relation 表

`relation_type_code`:空字符串 不是关联键, `OneToOne`, `ManyToOne`、 `OneToMany`, `ManyToMany` //其中 ManyToMany 才需要中间表
`mid_entity_code`:当 ManyToMany 时才会存在;且 命名为 `table1__table2` , 即双下划线分隔
// 在 Many 的那张表的 DbSchema 设置 foreignKey

### 其它注意点:

1.  `isShow`属性是 `getMetaList`(获取视图模型接口) 动态计算加上去的,数据库里面并不保存。前端是根据这个 isShow 来筛选字段的

## 修改页面级资源 的 模板配置(pageUiTemplateConfigs)的注意点

1.  页面资源中新增模板时,会自动在前端代码的 curRealPageKey 文件中导入模板组件;删除、排序之类操作也必须同步 curRealPageKey 文件
2.  排序的时候会对应更新 all-select 的 allTabObjHolderConfigs 配置数据 对应页面的 组件编号;不过通常情况不推荐模板重新排序