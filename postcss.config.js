const {
  uniPostcssPlugin
} = require('@dcloudio/uni-cli-shared')
module.exports = {
  plugins: [
    uniPostcssPlugin(),
    require('autoprefixer')(),
  ]
}

/* module.exports = {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
      'postcss-rem-to-responsive-pixel': {
        rootValue: 32,
        propList: ['*'],
        transformUnit: 'rpx'
      },
      //'../../postcss': {}
      'weapp-tailwindcss-webpack-plugin/postcss': {}
    }
  } */
